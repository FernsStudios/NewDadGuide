﻿import { Component } from '@angular/core';
import {  NavParams } from 'ionic-angular';
/**
 * if a user clicks on a todo item it will pass the info to this page and pop it open, shows more detail. maybe eventually an image?
 */
@Component({
  selector: 'page-todos-detail',
  templateUrl: 'todos-detail.html',
})
export class TodosDetailPage {
title;
description;
    constructor(public navParams: NavParams) {}
    ionViewDidLoad() {
        this.title = this.navParams.get('item').title;
        this.description = this.navParams.get('item').description;
    }
}