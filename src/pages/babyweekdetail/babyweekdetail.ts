import { Component } from '@angular/core';
import {  NavParams } from 'ionic-angular';
/**
 * 
 * This is the page pulled on the readmore button in the baby card on the home page, it pulls the string and image from the userweek
 */
@Component({
  selector: 'page-babyweekdetail',
  templateUrl: 'babyweekdetail.html',
})
export class BabyweekdetailPage {
userstate;
usertext;
userimg;
constructor(public navParams: NavParams) {}
    ionViewDidLoad() {
        this.userimg = this.navParams.get('userimg');
        this.userstate = this.navParams.get('userstate');
        this.usertext = this.navParams.get('usertext');
    } //pulls the user state info with navparams, displays the full Weekly Baby Update Text
}//end of code
