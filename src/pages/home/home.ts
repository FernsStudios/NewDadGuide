﻿import { Component} from '@angular/core';
import { NavController, NavParams, AlertController} from 'ionic-angular';
import firebase from 'firebase';
import { ProfileProvider } from '../../providers/profile';
import { AuthProvider } from '../../providers/auth-provider';
import { Profile } from '../profile/profile';
import { TodosPage } from '../todos/todos';
import { WeekDetailPage } from '../week-detail/week-detail';
import { BabyweekdetailPage} from '../babyweekdetail/babyweekdetail';
import {MomweekdetailPage} from '../momweekdetail/momweekdetail';
import {Login} from '../login/login';
import {DoctorpagePage} from '../doctorpage/doctorpage';
import { AdMobFree } from '@ionic-native/admob-free';
/* 
 *Home Page, MAIN PAGE OF APP, Lotsa code do big things
 */
@Component({
    selector: 'page-home', 
    templateUrl: 'home.html'
})
export class HomePage {

//for expander
items: any = [];
itemstwo: any = [];
itemsthree: any = [];
itemsfour: any = [];
itemsfive: any = [];
itemExpandHeight: number = 200;
babyitemExpandHeight: number = 300;
daditemExpandHeight: number = 200;
topitemExpandHeight: number = 300;
itemExpandHeighttwo: number = 200;

//variables for firebase refrences
database = firebase.database();
userDate: Date;
databaseDate: any;
public userProfile: any;
public birthDate: string
userDays: any;
userMonths: any;
userWeeks: any;
userYear;

//THESE ARE THE STATE VARIABLES
buttonClicked: boolean = false; disablebutton; usergender: any;
userLeft: any;
userleftheader: any;
firstuserlefttext: any;
shortfirstuserlefttext: any;
whatsnewusertext: any;
shortwhatsnewusertext;
userleftstate: any;
userstateBanner: any;
usermomtext: any;
shortmomtext;
userbabyimg: any;
userbabyimgtext: any;
public slides: { id: number, name: string }[];
firstTriText: any;
task;
//third tri var
now: any; oneweek: any; twoweek: any; threeweek: any; fourweek: any;
fiveweek: any; sixweek: any; sevenweek: any; eightweek: any; nineweek: any;
tenweek: any; elevenweek: any; twelveweek: any;
//second tri
thirteenweek: any; fourteenweek: any; fifteenweek: any;
sixteenweek: any; seventeenweek: any; eighteenweek: any; nineteenweek: any;
twentyweek: any; Toneweek: any; Ttwoweek: any; Tthreeweek: any; Tfourweek: any;
Tfiveweek: any; Tsixweek: any; Tsevenweek: any;
 //first tri
Teightweek: any; Tnineweek: any; thirtyweek: any;
toneweek: any; ttwoweek: any; tthreeweek: any; tfourweek: any;
tfiveweek: any; tsixweek: any; tsevenweek: any; teightweek: any;
tnineweek: any; fortyweek: any; foneweek: any; ftwoweek: any; fthreeweek: any;

constructor(public admob: AdMobFree,public navCtrl: NavController, public navParams: NavParams, public profileProvider: ProfileProvider, public authProvider: AuthProvider, public alertCtrl: AlertController) {
//each array is for a seperate expanded section, items=mom update, itemstwo=babyupdate, itemsthree=weeklyupdate
this.items = [{ expanded: false }];
this.itemstwo = [{ expanded: false }];
this.itemsthree = [{ expanded: false }];
this.itemsfour = [{ expanded: false }];
this.itemsfive = [{ expanded: false }];

}//end of constructor

//admob
 showBanner()
{
        this.admob.banner.config({
    id: ' ca-app-pub-5025420160808951~9389202427',
    autoShow: true,
})
    this.admob.banner.prepare()
}// this puts an admob banner across the bottom the screen, i need to set it up to happen periodically
    
//expansion functions
expandItem(item)
    {
        this.items.map((listItem) => {                
            if (item == listItem) {
                listItem.expanded = !listItem.expanded;
            } else {
                listItem.expanded = false;
            }                                                         
            return listItem;
        });
    }
expandItemtwo(item)
    {                         
       this.items.map((listItem) => {                  
            if (item == listItem) {
                listItem.expanded = !listItem.expanded;
            } else {
                listItem.expanded = false;
            }                                                         
            return listItem;
        });
    }
expandItemthree(item)
    {
        this.itemsthree.map((listItem) => {
            if (item == listItem) {
                listItem.expanded = !listItem.expanded;
            } else {
                listItem.expanded = false;
            }
            return listItem;
        }); 
    }
expandItemfour(item)
    {
        this.itemsfour.map((listItem) => {
            if (item == listItem) {
                listItem.expanded = !listItem.expanded;
            } else {
                listItem.expanded = false;
            }
            return listItem;
        }); 
    }
expandItemfive(item)
    {
        this.itemsfive.map((listItem) => {
            if (item == listItem) {
                listItem.expanded = !listItem.expanded;
            } else {
                listItem.expanded = false;
            }
            return listItem;
        }); 
    }
//end of expansion functions

//functions to push to other pages
logOut() 
    {   
        this.authProvider.logoutUser().then(() => {
            this.navCtrl.setRoot(Login);
        });
    }
profile()
    {
        this.navCtrl.push(Profile);
    }
todos() 
    {
         this.navCtrl.push(TodosPage, {
            userstate: this.userleftstate
        });
    }
weekDetail() 
    {
        this.navCtrl.push(WeekDetailPage, {
            item: this.userleftheader,});
    }


gotoDoctorPage()
    {
        this.navCtrl.push(DoctorpagePage);
    }//end of page push functions

//Trimester State Sets, need to exist outside the week elseif methods
thirdTrimester()
    {
        this.userleftstate = 'Third Trimester';
        this.userstateBanner = 'https://firebasestorage.googleapis.com/v0/b/due-date-counter.appspot.com/o/THIRD%20TRIMESTER.jpg?alt=media&token=47d14ce4-bcfd-4081-b070-b524d79709e5';
    }
secondTrimester()
    {
        this.userleftstate = 'Second Trimester';
        this.userstateBanner = 'https://firebasestorage.googleapis.com/v0/b/due-date-counter.appspot.com/o/THIRD%20TRIMESTER%20(1).jpg?alt=media&token=a4eb729a-d9ab-4582-abbc-f7c3a2268d17';
    }
firstTrimester()
    {
        this.userleftstate = 'First Trimester';
        this.userstateBanner = 'https://firebasestorage.googleapis.com/v0/b/due-date-counter.appspot.com/o/firstTrimester.png?alt=media&token=525867b7-c9fe-4165-a2b8-7cf3ef4279f7';   
    }
//end of trimester state sets

 //function for countdown timer
CountDownTimer(dt, id) 
    {
        //grabbing variables for math
        var end = new Date(dt);
        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;
   
        function showRemaining() {
            var now = new Date();
            var distance = end.getTime() - now.getTime();
            if (distance < 0) {
                clearInterval(timer);
                document.getElementById(id).innerHTML = 'We dont have your Due Date!';
                return;
            }
            //math 
            var days = Math.floor(distance / _day);
           
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            //assigning
            var countdownclock = document.getElementById(id);
         
            var daysSpan = countdownclock.querySelector('.days');
            var hoursSpan = countdownclock.querySelector('.hours');
            var minutesSpan = countdownclock.querySelector('.minutes');
            var secondsSpan = countdownclock.querySelector('.seconds');
            //connecting math and assignments
         
            daysSpan.innerHTML = days + '';
            hoursSpan.innerHTML = hours + '';
                minutesSpan.innerHTML = minutes + '';
                secondsSpan.innerHTML = seconds + '';
         
        }
    timer = setInterval(showRemaining, 1000);
    }//end of countdown function

//for gender button
setActive(gender) 
    { 
        if (gender == 'boy') {
            this.usergender = 'boy';
            this.disablebutton = true;
            this.profileProvider.updateGender(this.usergender);

        }
        else if (gender == 'girl')
        {
            this.usergender = 'girl';
            this.profileProvider.updateGender(this.usergender);
        }
    }
onButtonClickboy()
    {
        this.buttonClicked = !this.buttonClicked;
        this.usergender = 'boy';
        this.profileProvider.updateGender(this.usergender);
    }
onButtonClickgirl()
    {
       this.buttonClicked = !this.buttonClicked;
       this.usergender = 'girl';
       this.profileProvider.updateGender(this.usergender);
    }
onButtonClickneither()
    {
       this.buttonClicked = !this.buttonClicked;
       this.usergender = 'suprise';
       this.profileProvider.updateGender(this.usergender);
    }
//end of gender functions

//view week detail functions
viewWeek()
    {
        this.navCtrl.push(WeekDetailPage, {
            userstate: this.userleftstate,
            usertext: this.firstuserlefttext  
        });
    }
viewWeektwo()
    {
        this.navCtrl.push(MomweekdetailPage, {
            userstate: this.userleftstate,
            usertext: this.usermomtext  
        });
    }
viewWeekthree() 
    {
        this.navCtrl.push(BabyweekdetailPage, {
            userstate: this.userleftstate,
            usertext: this.whatsnewusertext,  
            userimg: this.userbabyimg
        });
    }



//IONVIEWDIDENTER Function, where we find the date, compare it to the dates we create, and set the text strings and images accordingly
ionViewDidEnter()
    {
    //this.task();
//grabbing current users profile info
    this.profileProvider.getUserProfile().then(profileSnap => {
    this.userProfile = profileSnap;

//creating date refrences, this has to be done in IonViewDidEnter so as to get the Current time
//third trimester, one week = one week FROM due date
            this.oneweek = new Date().getTime() + 604800000;
            this.twoweek = new Date().getTime() + 1209600000;
            this.threeweek = new Date().getTime() + 1814400000;
            this.fourweek = new Date().getTime() + 2419200000;
            this.fiveweek = new Date().getTime() + 3024000000;
            this.sixweek = new Date().getTime() + 3628800000;
            this.sevenweek = new Date().getTime() + 4233600000;
            this.eightweek = new Date().getTime() + 4838400000;
            this.nineweek = new Date().getTime() + 5443200000;
            this.tenweek = new Date().getTime() + 6048000000;
            this.elevenweek = new Date().getTime() + 6652800000;
            this.twelveweek = new Date().getTime() + 7257600000;
//second trimester, thirteenweek = thirteenweeks FROM due date
            this.thirteenweek = new Date().getTime() + 7862400000;
            this.fourteenweek = new Date().getTime() + 8467200000;
            this.fifteenweek = new Date().getTime() + 8467200000;
            this.sixteenweek = new Date().getTime() + 9676800000;
            this.seventeenweek = new Date().getTime() + 10281600000;
            this.eighteenweek = new Date().getTime() + 10886400000;
            this.nineteenweek = new Date().getTime() + 11491200000;
            this.twentyweek = new Date().getTime() + 12096000000;
            this.Toneweek = new Date().getTime() + 12700800000;
            this.Ttwoweek = new Date().getTime() + 13305600000;
            this.Tthreeweek = new Date().getTime() + 13910400000;
            this.Tfourweek = new Date().getTime() + 14515200000;
            this.Tfiveweek = new Date().getTime() + 15120000000;
            this.Tsixweek = new Date().getTime() + 15724800000;
            this.Tsevenweek = new Date().getTime() + 16329600000;
 //third trimester, Teightweek = twenty eight weeks FROM due date
            this.Teightweek = new Date().getTime() + 16934400000;
            this.Tnineweek = new Date().getTime() + 17539200000;
            this.thirtyweek = new Date().getTime() + 18144000000;
            this.toneweek = new Date().getTime() + 18748800000;
            this.ttwoweek = new Date().getTime() + 19353600000;
            this.tthreeweek = new Date().getTime() + 19958400000;
            this.tfourweek = new Date().getTime() + 20563200000;
            this.tfiveweek = new Date().getTime() + 21168000000;
            this.tsixweek = new Date().getTime() + 21772800000;
            this.tsevenweek = new Date().getTime() + 22377600000;
            this.teightweek = new Date().getTime() + 22982400000;
            this.tnineweek = new Date().getTime() + 23587200000;
            this.fortyweek = new Date().getTime() + 24192000000;
            this.foneweek = new Date().getTime() + 24796800000;
            this.ftwoweek = new Date().getTime() + 25401600000;
            this.fthreeweek = new Date().getTime() + 26006400000;
//firebase user date
            this.databaseDate = new Date(this.userProfile.dueDate).toDateString();
//grabbed the database millisecond number for due date, changed it to a local Date String, and now were gonna stuff it in a countdown timer
            this.CountDownTimer(this.databaseDate, 'countdown');
//millisecond to date string changes for header
            this.userDays = new Date(this.userProfile.dueDate).getDate();
            this.userMonths = new Date(this.userProfile.dueDate).getMonth()+ 1; //months are called 0-11 so we have to add one to make it accurate
            this.userYear = new Date(this.userProfile.dueDate).getFullYear();
            // more refs for countdown
            this.now = new Date().getTime();

/*=======COMPARISON LOOPS, SETTING STATE, TRIMESTER, FIRSTUSERLEFTTEXT, USERBABYIMG, USERBABYIMGTEXT, WHATSNEWUSERTEXT, USERMOMTEXT, SLIDES *                    
 /*-----------------------------THIRD TRIMESTER HERE--------------------------------------------------------------------- -------------------*/
if (this.userProfile.dueDate < this.now) {
                this.userleftstate = 'Third Trimester';
                this.thirdTrimester();
                this.userleftheader = "Week Forty One";

                this.firstuserlefttext = "Still here?!? Dont worry. Its normal for many babies to be born as a week after their due date. Make sure youre checking in with your doctor!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://media.mnn.com/assets/images/2016/01/cute-baby.jpg.838x0_q80.jpg';
                this.userbabyimgtext = "Your baby is the size of a full grown baby!";

                this.whatsnewusertext = "Hopefully your little baby is out and life as parents has begun!";

                this.usermomtext = "Mom is either ready for labor, or ready for recovery. Lend her a hand.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//week forty
else if (this.userProfile.dueDate <= this.oneweek) {
                this.userleftstate = 'Third Trimester';
                this.thirdTrimester();
                this.userleftheader = "Week Forty";

                this.firstuserlefttext = "Hopefully this week you aren't checking our app as much, you should have, or be having, a brand new baby! Oh well, we can talk about the stages of labor.\
                There are three stages of labor, and they can all last undetermined amounts of time. The first stage involves the cervix dialiating and mom-to-be beginning to have her contractions. As they speed up and become more intense you can roll into the second phase:Active labor. This stage can be anywhere from an hour to hours, this is when mom-to-be is actively pushing and giving birth.\
                There's a lot to think about during this phase: Do you want to record the birth on video? Will you want to cut the cord? (Be sure to remind your doctor or midwife if you do and be aware that some hospitals don't allow pictures or videos in the birthing room.)\
                Finally, after its all over, the third stage: I was just kidding! It's not over yet! This stage, which begins immediately after the birth of your baby and ends with the delivery of the placenta five to 10 minutes later, is usually anticlimactic but necessary\
Your partner may get a case of the chills or feel very shaky during this phase. If that's the case, be ready to offer a warm blanket and to hold your newborn while your partner's regaining her strength.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://nameberry.com/blog/wp-content/uploads/2016/10/shutterstock_464300804-776x600.jpg';
                this.userbabyimgtext = "Your baby is the size of a full grown baby!";

                this.whatsnewusertext = "Your baby measures 14.3 inches (36.2 cm) from crown to rump and weighs 7.6 lbs (3.5 kg).\
                If they haven't come out already, then they will be any day now! Most babies are born with two weeks of their due date, as it can be slightly dangerous to wait longer.\
                Howver it is not uncommon for a baby to be born in the 41st or 42nd week, so keep talking to your doctor, and helping mom-to-be relax!";

                this.usermomtext = "At this point mom-to-be is all done growing! All she has to do now is play the waiting game. Make sure she is getting lots of rest, as soon she will be going through some serious workouts.\
                ";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//39
else if (this.userProfile.dueDate < this.oneweek) {
                this.userleftstate = 'Third Trimester';
                this.thirdTrimester();
                this.userleftheader = "Week Thirty-Nine";

                this.firstuserlefttext = "Now we might be getting a little impatient. We might also be getting a little frustrated, labor signs can be extremely difficult to read, especially for first-time parents. Late in pregnancy, many women havve painful contractions that may feel like false labor — Braxton Hicks contractions that may start out strong but taper off and then stop after a while. Look for these signs, among others, that your partner is experiencing the real deal:\
                1) Her Water breaks. This is a big one, obviously, whether its a trickle or a strong flow you need to call the hospital ASAP. 2)Persistent lower back pain, especially if your partner also complains about a crampy, premenstrual feeling.\
                3)She passes the mucus plug, which is in the cervix. This isn't necessarily a sign that labor is imminent – it could still be several days away. But at the very least, it indicates that things are moving along.\
                4)Contractions that occur at regular and increasingly shorter intervals and become longer and stronger in intensity. None of these can be a gurantee that anything is happening right now, and labor can be a multi day process! So be patient and try to help out mom-to-be in whatever way she needs. ";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://www.decorcentral.com/images/D/F700-L-01-01.jpg';
                this.userbabyimgtext = "Your baby is the size of a rack of ribs!";

                this.whatsnewusertext = "Your baby measures 13.9 inches (35.2 cm) from crown to rump and weighs 7.2 lbs (3.3 kg).\
                This week your baby is considered full term! His/her baby skin is growing in, and theyre listening to your voices while his brain continues to grow even larger!\
                Continue watching for those signs of labor, it could happen any day now!";

                this.usermomtext = "At this point mom-to-be is all done growing! All she has to do now is play the waiting game. Make sure she is getting lots of rest, as soon she will be going through some serious workouts.\
                ";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//38
else if (this.userProfile.dueDate < this.twoweek) {
                this.userleftstate = 'Third Trimester';
                this.thirdTrimester();
                this.userleftheader = "Week Thirty-Eight";

                this.firstuserlefttext = "So now we have our bags packed, our arms are sore from our vaccines, and theres not much left for you to do but wait, and wait. and wait. \
                But we can practice installing the car seat before we're in the hustle and bustle of labor. Because there are so many car-seat manufacturers and models, it's difficult to provide specific instructions about the safe installation of any one of them. In addition, it's nearly impossible to stay on top of which ones are safe to begin with. Luckily, there are a number of valuable resources at your disposal.\
                One of the best resources is the NHTSA website, with a constantly updated database of information for many makes and models of car seats.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://metrouk2.files.wordpress.com/2015/02/kfc-bucket.png';
                this.userbabyimgtext = "Your baby is the size of a bucket of fried chicken!";

                this.whatsnewusertext = "Your baby measures 13.4 inches (34.1 cm) from crown to rump and weighs 6.8 lbs (3.1 kg).\
                Both your babies eyes, and his or her hair(if they have any) now has color! At this point your baby is getting ready to come meet you, so the main signs your watching for are labor signals.\
                Your baby is still working away creating surfacatant, a substance that will cause his/her air sacs in their little lungs inflate once he is out and breathing in air.";

                this.usermomtext = "At this point mom-to-be is all done growing! All she has to do now is play the waiting game. Make sure she is getting lots of rest, as soon she will be going through some serious workouts.\
                ";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//37
else if (this.userProfile.dueDate < this.threeweek) {
                this.userleftstate = 'Third Trimester';
                this.thirdTrimester();
                this.userleftheader = "Week Thirty-Seven";

                this.firstuserlefttext = "Lets start getting that bag ready Dad. Mom-to-be probably already has hers, and maybe the babies, and maybe even yours, primed and ready to go, but lets talk about what you need at the hospital.\
                1) You need your watch. Preferably one that you can read. Trust me, you want to know the time. 2)Wallet. This seems obvious, but it wasn't for me apparently. 3)More then one change of (comfortable) clothes. You're going to be there for awhile.\
                4)Cash for the vending machines(which may or may not be working). 5)Cell phone chargers. Clearly. 6)Entertainment. The extremely small hospital tvs with basic cable aren't going to cut it for three plus days. ";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'http://s.hswstatic.com/gif/power-drill-1.jpg';
                this.userbabyimgtext = "Your baby is the size of a power drill!";

                this.whatsnewusertext = "Your baby measures 13.0 inches (33.1 cm) from crown to rump and weighs 6.3 lbs (2.9 kg).\
                 As you approach the finish line, your babys lungs and reflexes are whirring up to full speed. At this point almost babies born in pre-term labor survive outside the womb!\
                These last few weeks of baby cooking are going to help add those last couple pounds of fat and get them ready to handle the harsh reality that is life. (They might have a few years for that)\
               ";
                

                this.usermomtext = "Mom-to-be at this point is very close to her final birth weight, luckily for her. As you get closer to the due date be sure to help her be comfortable and get as much rest as possible, labor is going to take a lot of energy.\
                Mom-to-bes cervix is beginning to dialate and thin out to prepare for child birth, but she can't feel that. Be sure to keep up with your doctor appointments, and keep mom-to-be as relaxed as possible. ";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//36
else if (this.userProfile.dueDate < this.fourweek) {
                this.userleftstate = 'Third Trimester';
                this.thirdTrimester();
                this.userleftheader = "Week Thirty-Six";

                this.firstuserlefttext = "Before you bring home your new baby, its important to make sure you, mom-to-be, and anyone else who will be in regular contact with your new baby is fully vaccinated!\
                There are many vaccines, (flu, MMR, chicken pox, tdap, hep A, HPV, etc) so be sure to talk to your doctor about what you need, and what you need to stay up to date on. We're getting very close, you're one short month from your due date!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = '';
                this.userbabyimgtext = "Your baby is the size of a honeydew melon!";

                this.whatsnewusertext = "Your baby measures 12.6 inches (32.1 cm) from crown to rump and weighs 5.8 lbs (2.6 kg).\
                The only major system your baby has left to finish cooking up is his/her digestive system! The baby has been working hard to get ready for that first poop, and practicing swallowing and hiccuping away to get ready for those first few days out in the real world.\
                Your baby should be in the 'head down' position at this point for birth, and mom-to-be should be as uncomfortable as ever!\
                ";

                this.usermomtext = "Lots of symptoms should still be hitting mom-to-be, possibly including, but not limited \
                to: Pelvic Discomfort, Bladder Pressure, Trouble breathing, Heartburn, Insomnia, Headaches, Backaches, Braxton-Hicks, and Swollen feet.\
                Take a second and enjoy the fact that you are a Dad. At this point you should be helping mom-to-be watch her symptoms, and probably assist her in just moving around. Its not easy.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//35
else if (this.userProfile.dueDate < this.fiveweek) {
                this.userleftstate = 'Third Trimester';
                this.thirdTrimester();
                this.userleftheader = "Week Thirty-Five";

                this.firstuserlefttext = "We are now five short(maybe) weeks from your due date!\
                You really need to start reading up on things like contractions, water breaking, cervical dilation, etc.\
                You may want to prepare yourself by seeing a video of a birth during childbirth classes, although I wasn't that brave.\
                ";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://previews.123rf.com/images/kozzi/kozzi1301/kozzi130114668/17494426-Close-up-of-dozen-of-eggs-in-carton-box-isolated-over-white-background-Stock-Photo.jpg';
                this.userbabyimgtext = "Your baby is the size of a carton of eggs!";

                this.whatsnewusertext = "Your baby measures 12.2 inches (31.1 cm) from crown to rump and weighs 5.3 lbs (2.4 kg)!\
                He or she is starting to run out of room inside your mom-to-be, and he may start to move a little less, although his kick counts should remain the same.\
                His/her kidneys are fully developed now, and the liver can process some waste products. At this point most of the physical development has finished, and its just a matter of putting on weight before he or she comes out to meet you!";

                this.usermomtext = "Mom-to-be has to be feeling a little out of room by now. Her utuerus now reaches up to under her rib cage.\
                Because of this, the other internal organs are all being crowded out to allow room for your growing baby.\
                She may be starting to have some of the fun pre labor symptoms, such as losing her mucas plug, or some 'leaking'. If you think at any point your water is breaking, go to the hospital! They will be able to check and reassure you everything is okay.\
                  At this point mom-to-be should be going to the doctor every week, and doing her kick counts every day!";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//34
 else if (this.userProfile.dueDate < this.sixweek) {
                this.userleftstate = 'Third Trimester';
                this.thirdTrimester();
                this.userleftheader = "Week Thirty-Four";

                this.firstuserlefttext = "We're six weeks out, so lets talk $$$$\
                Assess your finances and think about what kind of obligations you'll have once the baby is born. Talk to your partner about what bills are going to come when the baby comes, and how your lifestyles are going to change to reflect that.\
                Early planning can help soften the financial blow of fatherhood. Remember,\
                at first the baby will not be breaking your bank, unless mom is weak for those onesies. Most are.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://assets.academy.com/mgen/51/10105251.jpg';
                this.userbabyimgtext = "Your baby is the size of a baseball glove";

                this.whatsnewusertext = "Your baby measures 11.9 inches (30.2 cm) from crown to rump and weighs 4.7 lbs (2.1 kg) this week.\
                The vernix caseosa, the coating that will help your baby 'slip' out. He/she is peeing a whole pint a day, and getting ready for that first poop!\
                He/she's central nervous system is maturing, and her lungs are continuing to mature as well.";

                this.usermomtext = "";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//33
else if (this.userProfile.dueDate < this.sevenweek) {
                this.userleftstate = 'Third Trimester';
                this.thirdTrimester();
                this.userleftheader = "Week Thirty-Three";

                this.firstuserlefttext = "Well, your nursery is done, the boss is ready for your maternity leave, and mom and baby are coasting along. Whats next?\
                Its probably a great time, although most parents will already have, to start talking about some of those big decisions you have to make together. \
                Circumcision? Pacifiers? Breastfeeding? THE NAME? These aren't things you want to talk about in the delivery room. Have some nice calm conversations in the evenings about how you want to approach these things, and have a plan.(dont forget sex is still safe, after the talk)";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://www.healthy-pet.com/sites/default/files/ferret-page-ferret.jpg';
                this.userbabyimgtext = "Your baby is the size of a ferret!";

                this.whatsnewusertext = "Your baby measures 11.5 inches (29.3 cm) from crown to rump and weighs 4.2 lbs (1.9 kg).\
                The baby has almost reached the point where they don't get bigger, and their bones are hardening up even more so they can support themselves when they come out of the womb.\
                At this point some of our weight and heights are more guesstimations then anything else, as babies size on delivery can obviously vary to a large degree.\
                Your doctor will be a better source of knowledge on that specifically then we can! That brain is still growing fast, and those eyes are open and looking around whenever the baby is awake!\
                Its almost time!";

                this.usermomtext = "Mom-to-be is probably going to the doctor every week or every other week at this point, so your Doctor will have great advice specifcally for you two.\
                But mom is probably running out of room for that baby, and its definetly holding her up. Your little baby is probably keeping her up most nights now, either pracicing the NutCracker, or kicking mom-to-bes bladder for fun.\
                If you haven't already, maybe check out pregnancy pillows, and once again....sex is safe.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//32
else if (this.userProfile.dueDate < this.eightweek) {
                this.userleftstate = 'Third Trimester';
                this.thirdTrimester();
                this.userleftheader = "Week Thirty-Two";

                this.firstuserlefttext = "Eight weeks left, thats only two months!\
                The last seven months have passed in a blur, but hopefully you're feeling pretty ready for all this.\
                Although you should have already, make sure at this point that you have talked to your employer about your maternity leave.\
                As the birth date can be extremely unpredictable, its good to keep your boss in the loop, and at this point wrap up anything at work that you are vitally needed for.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://s-media-cache-ak0.pinimg.com/originals/1a/7e/0e/1a7e0eefdd8cbf25c8aefc9745a73ec3.jpg';
                this.userbabyimgtext = "Your baby is the size of an armadillo!";

                this.whatsnewusertext = "Your baby measures 11.1 inches (28.3 cm) from crown to rump and weighs 3.8 lbs (1.7 kg).\
                He/she has probably become an old pro at sucking their thumb and hiccuping by now, hopefully you got to feel some of it dad. \
                Baby has a full set of fingernails and toenails now, and is probably dancing like crazy. Most babies at this point will be 'head down', to prepare for delivery.\
                At this point, that viability percentage we talked about in the Second trimester has hit 90%! Thats some pretty good chances.\
                Keep feeding mom and baby well, and finish up that nursery!";

                this.usermomtext = "At this point, the size of your incoming baby is probably starting to hurt mom-to-be's back.\
                Imagine if, 24/7, you were wearing a backpack filled with 20 pounds of stuff, except you are also wearing the backpack backwards.\
                And it moves. And makes you nauseous. And kicks you in weird places. And makes you pee twenty times a day.\
                So her complaints are pretty legimiate. Try to be there for her, and remember, the end is near!";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//31
else if (this.userProfile.dueDate < this.nineweek) {
                this.userleftstate = 'Third Trimester';
                this.thirdTrimester();
                this.userleftheader = "Week Thirty-One";

                this.firstuserlefttext = "Did you know you have less then ten weeks left before your due date?\
                This week, you should probably  start researching every possible path to the hospital.\
                From the house, from work, from the store, from the highway, etc. Go to the hospital on your lunch and research the parking garages at the hospital.\
                You don’t want to be incredibly efficient getting to the hospital and have no clue where to actually park the car.\
                Many hospitals also offer birth tours, where you and mom-to-be can come in and see the rooms, get a tour, and get a rundown on how its all going to go.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'http://a.espncdn.com/combiner/i?img=/redesign/assets/img/icons/ESPN-icon-football-college.png&w=288&h=288&transparent=true';
                this.userbabyimgtext = "Your baby is the size of a football!";

                this.whatsnewusertext = "Your baby measures 10.8 inches (27.4 cm) from crown to rump and weighs 3.3 lbs (1.5 kg).\
                Your baby is just a small bit away from its birth height! The neck is more flexible, allowing him/her to turn that head, dancing with even more finesse.\
                All the senses are now intact, these last few weeks are about finishing up and fattening up for delivery. The immune system is almost ready for the big outside world, it will just need a little bit of time in the outside world,\
                The brain is still growing, although the skull has not fused yet, and won't until after delivery. Keep talking to your baby, they're definetly listening now. Many pregnant parents report that their baby will respond to their voices!";

                this.usermomtext = "Mom-to-be is probably enjoying some braxton-hicks on a regular basis at this point, but if she is having more then four in a hour, they might not be braxton hicks, and you should get to the doctor and get that checked out.\
                Now that we covered the alarming part, some moms may start to be leaking a bit of yellowish discharge from their breasts at this point. This is Colostrum, your babies first food, filled with nutrients and other junk that help out that immune system we just talked about.\
                Make sure you're still fufilling those pregnancy cravings, and help mom-to-be to be resting often, she needs to start saving some energy!";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//30
else if (this.userProfile.dueDate < this.tenweek) {
                this.userleftstate = 'Third Trimester';
                this.thirdTrimester();
                this.userleftheader = "Week Thirty";

                this.firstuserlefttext = "Thirty weeks! Im sure it felt like a blur. If you've been participating in a prenatal class, you've acquired a good foundation for the immediate future with your partner and baby.\
                More important, if you've been part of a prenatal class with your partner you can begin prepare as a couple to share the birth experience.\
                You should begin discussing the role you'll take during birth with your partner.\
                You may feel some distress at your partners newly increased pains or stressed, but stay calm. Although your trying hard theres only so much new parents can do to preapre for birth or getting their home ready for its arrival;\
                There are always a few things that feel incomplete, but its important to remember this is a happy exciting time, don't get caught up in worrying!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'http://www.slate.com/content/dam/slate/articles/health_and_science/wild_things/2015/06/150622_WILD_Platypus.jpg.CROP.promo-mediumlarge.jpg';
                this.userbabyimgtext = "Your baby has reached the size of a platypus!";

                this.whatsnewusertext = "Your baby measures 10.4 inches (26.5 cm) from crown to rump and weighs 2.9 lbs (1.3 kg).\
                At this point, the babys lanugo, the furry cover of hair should start to be falling off completly as she/he now has enough fat to maintain a body temp.\
                From this week on, the baby should be adding a half pound every single week. The eyes have reached their final destination, but they havent finishe developing. Your baby can now blink! but their vision is still poor.\
                Within the next few days. most babies begin to 'drop', moving from the breech position to the birth position, and lowering their heads, as they are delivered first.\
                The baby should still be dancing it up, but soon they will start running out of room and they might move slightly less. Dont be afraid to ever go in if you are worried about your babies movements though.";

                this.usermomtext = "Mom-to-be's pregnancy symptoms should be back in the full swing you might remember from the First trimester.\
                She may also be nesting, worrying about having everything ready for the babies room, and breastfeeding, etc. Indulge her, and help her to feel relaxed. Remember she needs to not be stressed, and neither should you Dad!\
                If she hasn't already, she may be having a baby shower soon! This will translate to many free diapers and baby goods, so be sure to encourage this. ";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//29
else if (this.userProfile.dueDate < this.elevenweek) {
                this.userleftstate = 'Third Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Twenty-Nine";

                this.firstuserlefttext = "Things are heating up! In less then 90 days, you are going to be a father.\
                Now is the time to start checking that to-do list and seeing what you have left.\
                Is the nursery done? Have you two decided on a name? What about your whooping cough shot dad? As you get closer, you want to remove as much stress from yours and your partners life as possible, so as to be ready for your newborn.\
                The sooner you knock out those last few things, the less you have to worry about as we reach the end of this trimester.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'http://www.keenforgreen.com/sites/default/files/milk-jug.jpg';
                this.userbabyimgtext = "Your baby has reached the size of a one gallon milk jug!";

                this.whatsnewusertext = "Your baby measures 10.1 inches (25.6 cm) from crown to rump and weighs 2.5 lbs (1.2 kg) this week!\
                They are probably moving more and more, and the movements can feel more violent. You should be to able to feel, and maybe see many of them this month, if you're paying attention to mom-to-be's tummy.\
                The babies muscles and lungs are continuing to grow fast and strong, and he will soon be able to beat you in an arm-wrestling match. ";

                this.usermomtext = "As you and your partner move towards the end of the pregnancy, you can help out by continuing to keep the family diet healthy.\
                Right now protein, Vitemin C, iron, and folate, and possibly fiber, are all important and helpful for your baby, and mom-to-be.\
                At this point, her body is releasing hormones that are relaxing her muscle tissues, to prepare her for delivery. Acid reflux hits hard at this point for many, and be sure to talk to your doctor about which OTC medication to take. (We relied on TUMS)\
                Pregnancy brain will be at its strongest now, as will headaches. Lastly, make sure mom-to-be is staying extremely hydrated. Remember, shes eating and drinking for two!";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//28
else if (this.userProfile.dueDate < this.twelveweek ) {
                this.userleftstate = 'Third Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Twenty-Eight";

                this.firstuserlefttext = "Welcome to the Third Trimester! Unlike the second, this one is going to start being rough again for your mom-to-be.\
                As the third trimester of the pregnancy dawns, so does the realization that this journey will culminate in a baby very soon. All the planning and preparations are for this one purpose and it can be both an exciting and slightly alarming conclusion for the expectant dad.\
                As the delivery draws near, the bright and buoyant woman of the second trimester has faded. Your partner has, once again, become easily tired and has little energy for much of anything. This is due to the baby’s increased growth and the demands the added weight makes on your partner’s frame.\
                Be ready for the next few weeks, and try to be supportive. But dont forget to get excited, you're very near the end!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://d3nevzfk7ii3be.cloudfront.net/igi/W2DeZjnySeGvooBS.large';
                this.userbabyimgtext = "Your baby is the size of the original Super Nintendo!";

                this.whatsnewusertext = "Your baby measures 9.7 inches (24.7 cm) from crown to rump and weighs 2.2 lbs (1.0  kilogram)!\
                Your baby is rapidly approaching birth size, and mom-to-be is probably noticing it. The little one continues to pack on fat, helping to regulate his/her body tempreature, a neccesity for him/her to survive outside of the womb. \
                The baby is now dreaming in their sleep cycles, and practicing breathing, coughing, hiccuping, and keeping mom awake all night. ";

                this.usermomtext = "As we enter the third trimester, you're going to begin to have terrible flash backs of the early weeks.\
                Symptoms of nausea, headaches, backpain, and more braxton hicks get more common once again in the Third trimester. At this point your doctor should be seeing you at least every two weeks.\
                This or next week mom-to-be's body may start lactating, which is a good time to start thinking about whether you want to breastfeed!";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }



 /*-----------------------------SECOND TRIMESTER HERE--------------------------------------------------------------------- -------------------*/
                //week27
else if (this.userProfile.dueDate < this.thirteenweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Twenty-Seven";
                this.firstuserlefttext = "This is the final week of the Second Trimester! Its a been long, maybe boring couple of months, hasn't it?\
                Once you have the baby, this seems like a million years ago, but as we go into the Third trimester remember to keep communicating with your mom-to-be about how shes feeling.\
                If you haven't already decided, this is a great time to start thinking about a name!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'http://www.traditionaloven.com/foods/img/vegetables/cauliflower_whole_raw_2.jpg';
                this.userbabyimgtext = "Your baby is the size of a head of cauliflower!";

                this.whatsnewusertext = "Your baby measures 9.4 inches (23.8 cm) from crown to rump and weighs 1.9 lbs (875 g).\
                At week 27 of pregnancy, your baby is sleeping more than 95% of the day. Your baby grows while asleep so it's not surprise that sleep is the predominate activity.\
                You should be able to feel your babies kicks through mom's belly by now, and probably his/her hiccups as they get ready for breathing.";

                this.usermomtext = " Mom-to-be is entering the Third trimester next week! From here on out life becomes a chore for her and difficult to do in her lither days.\
                Bending over, getting up, moving, going to the bathroom, everything is going to be harder for her in the coming weeks and its up to you to help her out.\
                This is a good time to begin doing kick counts, after a meal, have mom lie down and count your little ones movements. The goal here is around 10 in two hours, but dont let yourself stress over this too much. ";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            } //week26
else if (this.userProfile.dueDate < this.fourteenweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Twenty-Six";
                this.firstuserlefttext = "We're very near the end of the Second trimester, which means this relatively easy period will soon end! This week is a great time to start thinking about that nursery. \
                Pretty soon, mom-to-be is not going to be able to help(physically, she will always be supervising) set up the nursery furniture. So if you haven't yet, why don't the two of you go pick out a bassinet?";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'http://cdn1.gluestore.com.au/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/l/a/lac_6300005031929_black04.jpg';
                this.userbabyimgtext = "Your Baby is the Size of Wide-Brimmed Fedora!";

                this.whatsnewusertext = "Your baby measures 9.0 inches (22.8 cm) from crown to rump and weighs 1.7 lbs (760 g).\
                This week our viability number is 85%! Your baby is continuing to get plumper, building up that baby fat so theyre cute and chubby when they come out.\
                At the same time, your baby's baby fuzz has also started to fall off since lanugo is not needed to keep your baby warm now that there is baby fat.\
                So close to the end!";

                this.usermomtext = "At 26 weeks, mom-to-bes uterus is about two-and-a-half inches above her belly button, and everyone should be noticing that bump now.\
                There should be a lot of baby activity right now, which can make it hard to sleep,\
                or sit, or do much of anything. As she moves into the last trimester, you're going to be missing this sweet peaceful time.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//25
else if (this.userProfile.dueDate < this.fifteenweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Twenty-Five";
                this.firstuserlefttext = "Take a deep breath, this is the calm before the storm.\
                Once you hit the third trimester, the rest of pregnancy seems to pass quickly (except, of course, for those last few restless nights before delivery).\
                Enjoy some quality time with your mom-to-be, and encourage her in the activities she enjoys. Now is a good time for travel, exercise and doing things you both love to do, before time runs out and you have a newborn!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://babysizer.com/img/sizer/cravings-25.jpg';
                this.userbabyimgtext = "Your baby is the size of a popped bag of popcorn!";

                this.whatsnewusertext = "Your baby measures 8.6 inches (21.8 cm) from crown to rump and weighs 1.5 lbs (660 g).\
                Last week, we learned we were at 50% viability outside of the womb. By this week, that number has gone up to 65%!\
                This week your babies eyes will open the first time, and they will probably begin kicking mom-to-be whenever they see a bright light.\
                Along with this, their brain is growing rapidly. \
                Most of the growth your baby is experiencing this week up through, and beyond birth, is the formation of new wrinkles.\
                With each new wrinkle comes more surface area and more surface means more connections from one part of the brain to another.\
                More folds means more smarts. That's one of the things that separates humans from other mammals.";

                this.usermomtext = "As we near the last trimester, mom-to-be might be experiencing even more Braxton Hicks, along with another sympton that many women dont love.\
                Three out of four women may experience something called linea nigra, Latin for black line,\
                on their tummies during the second trimester. It appears as a brownish line that runs vertically down mom's belly and can run from as high up as the top of the belly down to the pubic area.\
                The line forms due to a bunch of hormones that are called MSH, being produced by her placenta. This same hormone makes her nipples and aerolas darken, but don't worry. It will dissapear after pregnancy.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//24
else if (this.userProfile.dueDate < this.sixteenweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Twenty-Four";
                this.firstuserlefttext = "Have you maybe been feeling a little left out and helpless during some of the pregnancy process?\
                Feeling left out or unimportant is a pretty common feeling in pregnancy. You might even feel that your partner isn’t paying you as much attention as she used to.\
                But having a baby growing inside you is an amazing thing, and it’s normal for her to feel absorbed in the experience. She isn’t rejecting you, she just might be a little distracted. \
                If you know another dad or expectant dad, you could ask him about his experience. Did he feel left out at times during his partner’s pregnancy? Was he given any advice that helped him through?";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://static1.squarespace.com/static/569024f8841aba3b66d67c55/t/56ad4e6e8259b57c9286d43e/1454198402344/';
                this.userbabyimgtext = "Your baby is the size of a cantelope!";

                this.whatsnewusertext = "Your baby measures 8.2 inches (20.8 cm) from crown to rump and weighs 1.3 lbs (600 g).\
                At week 24 your baby just crossed an important threshold — 50% fetal viability. Fetal viability is the probability that your baby could survive outside the womb if it were born this week.\
                Although that sounds like a depressing statistic, the fact is that you have reached the point where much less could go wrong from this point on.\
                The reason for this is that your baby is developing their  bronchi and bronchioles, which is analogous in looks to the branches and leaves of a tree, respectively.\
                These branches help deliver the air to your babies new lungs!";

                this.usermomtext = "As we near the last trimester, mom-to-be might be experiencing even more Braxton Hicks, along with another sympton that many women dont love.\
                Three out of four women may experience something called linea nigra, Latin for black line,\
                on their tummies during the second trimester. It appears as a brownish line that runs vertically down mom's belly and can run from as high up as the top of the belly down to the pubic area.\
                The line forms due to a bunch of hormones that are called MSH, being produced by her placenta. This same hormone makes her nipples and aerolas darken, but don't worry. It will dissapear after pregnancy.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//23
else if (this.userProfile.dueDate < this.seventeenweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Twenty-Three";
                this.firstuserlefttext = " You might be wondering what to do now for your mom-to-be. The middle part is always super weird, because that first tri' was packed with doctors appointments, scans and information from everyone.\
                And it's the same at the end , but the middle bit is kinda radio silence. But that's okay, because you're past the danger zone and there's not a whole lot you can do.\
                Just make sure your mom-to-be is eating right, getting plenty of rest, some low-impact exercise - all that good stuff.\
                Being anxious is totally fine, but there's really nothing to worry about at this stage. There's nothing wrong with getting a scan if you want, but you'll also be surprised how quickly this part goes by.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'http://www.seriouseats.com/images/2012/10/20121009-mac-n-cheese-box-kraft.jpg';
                this.userbabyimgtext = "Your baby is the size of a box of Macaroni and cheese!";

                this.whatsnewusertext = "Your baby is measuring 7.8 inches (19.7 cm) from crown to rump and weighs 1.1 lbs (501 g).\
                The muscles in your babies jaw are continuing to develop, as he swallows down his own amniotic fluid and poop.\
                Your baby also has tastebuds now, and can taste everything that mom is putting in her belly. And he/she will also probably make it very clear to her if they dont like something she ate.\
                The heart is also growing much stronger, this week it may be possible to hear the heartbeat with a stethoscope!";

                this.usermomtext = "Mom-to-be may be noticing something strange, also known as pregnancy brain.\
                If she finds herself overly forgetful, and unable to keep her mind straight dont fear.\
                This is a totally normal thing for alot of women in pregnancy.\
                She may also find her gums getting sensitive, and her mouth bleeding more often when she brushes her teeth.\
                Once again, totally normal, along with a myriad of other symptoms she might be experiencing as we head towards the Third Trimester";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//22
else if (this.userProfile.dueDate < this.eighteenweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Twenty-Two";
                this.firstuserlefttext = "As we near the Third trimester, its time for you and mom-to-be to start thinking about a birth plan.\
                The great thing about births today is that there are many choices of settings, pain relief, personnel, etc.You can basically plan a birth like you plan a wedding.The downside is that while most weddings go off according to plan, births tend to take on a life of their own.\
                The birth plan can address any issue that you want to be clear on with those involved in the birth, including whether she wants to have an epidural, who you plan to have attend during the birth, your wishes on a C-section, etc.\
                Childbirth education classes will provide a great deal more information on birth choices and planning.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://static.giantbomb.com/uploads/original/1/17020/545887-pipboy.jpg';
                this.userbabyimgtext = "Your baby is now the size of a pip-boy!";

                this.whatsnewusertext = "Your baby measures 7.3 inches (18.6 cm) from crown to rump and weighs 15.2 oz (430 grams).\
                Your baby has started to put on the pounds, but has yet to fill up the excess skin that has grown so far.\
                Your babies eyes are now fully formed as well, but the irieses will remain uncolored. Without much else going on, the baby has probably taken to dancing the day and night away, much to mom-to-bes chagrin";

                this.usermomtext = "Mom-to-bes belly is continuing to grow, as is the rest of her. Her belly button may be may begin to dissapear as her bump grows, an interesting sight.\
                Her nipples are aereolas are going to become much bigger over the next few weeks, as her body prepares for breastfeeding.\
                Another common sympton for pregnant women at this point is feet pain and swelling. It might be time to switch to sandals and hot water baths from you to her.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//21
else if (this.userProfile.dueDate < this.nineteenweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Twenty-One";
                this.firstuserlefttext = "Now that we are in week Twenty one you probably will either have your twenty week scan this week or already have!\
                At the 20- week scan, you’ll probably see your baby’s heart beating, the curve of baby’s spine, baby’s face, and baby’s arms waving and legs kicking.There might even be some cute thumb- sucking.\
                Because you can see so much in this scan, you might have an even stronger sense of the baby or babies coming into your life.\
                Usually you can get an ultrasound photo or even a DVD to share with family and friends.\
                There’s a small chance that the scan might pick up a serious health problem or complication.Some abnormalities won’t be seen on a scan at all or can’t be seen until later in the pregnancy.\
                Genetic conditions like Down syndrome can be diagnosed only by special antenatal tests and checks such as amniocentesis.If you’re thinking about having these tests done, genetic counselling services can give you more information about them.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://s-media-cache-ak0.pinimg.com/originals/9b/ee/50/9bee50a82d164e8220a459580375cf55.jpg';
                this.userbabyimgtext = "Your baby is about the size of an axe head!";

                this.whatsnewusertext = "Your baby now measures 6.9 inches (17.5 cm) from crown to rump and weighs 12.7 oz (360 g).\
                You might be saying to your screen right now,My other pregnancy apps and sites  say my baby's 4 inches bigger. While they aren't wrong, they're also complicating things.\
                Here's why: 6.9 inches is a measure from crown to rump, which means from the top of your baby's head to their bottom.\
                At week 21 of pregnancy, instead of measuring a baby from crown to rump,  they start to use crown to heel. The crown-heel length (CHL) is calculated by measuring from a babys head to their feet; which is where the extra 4 inches comes from.\
                At week 21, all your baby's critical organs have now formed to some varying degree. While there is still a lot of growing to be done, it's about getting bigger and stronger now. Having said that, the focus of this week is mostly on them down there parts.\
                If your baby is male, his testicles will be descending from his abdomen into his scrotum.Think Times Square ball drop on New Years Eve or bungee jumping off a bridge.\
                If your baby is female, then this is the week her vagina begins to form.";

                this.usermomtext = "Once again, not a lot is new with your mom-to-be this week, so lets talk about pregnancy sex.\
                Some pregnant women feel their sexual desire skyrocket when they're pregnant, at least part of the time.\
                They may enjoy the increased blood flow to the pelvic area and the heightened sensitivity to stimulation that this brings, as well as the increased vaginal lubrication due to hormonal changes.\
                But it's also completely normal not to crave sex. So either way, try to hang in there big guy. ";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//20
else if (this.userProfile.dueDate < this.twentyweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Twenty";
                this.firstuserlefttext = "Week Twenty! You are probably starting to really feel the stress of a looming baby, but remember to hold on and stay calm for mom-to-be.\
                You must learn the mind-blowing power of patience.\
                Everything is going to become a thousand more times difficult for her during the end- times of pregnancy.She might get really frustrated and sad about it all.\
                Be cool.Be helpful. Also, understand that you are not prepared.Nothing can really truly prepare you for dad- dom except experience.Relax.You're not abnormal in this, it's totally fine.\
                By being patient and open to being wrong and making little mistakes, you'll do just fine.\
                Sounds backward and ironic, I know, but it's like playing MegaMan. You mess up a lot (and die in the game), until you learn the pattern and the method to succeed.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://image.freepik.com/free-photo/pint-of-beer_2317035.jpg';
                this.userbabyimgtext = "Your baby is the size of a pint(none for mom-to-be).";

                this.whatsnewusertext = "Your baby now measures 6.5 inches (16.4 cm) from crown to rump and weighs 10.6 oz (300 grams).\
                You have now made it halfway through the pregnancy! Or at least, mom-to-be has. You have a few months more before your real work begins.\
                This week, your baby learns to swallow and this skill will help your baby swallow  milk or formula when the time comes.\
                Your baby may also start to produce something called meconium as part of their rudimentary digestive process.\
                Meconium is a blackish, dark greenish sticky tar-like goop that is literally crap. If you didn't think sebum was appetizing last week, you're not going to like this any better.\
                Meconium is made up lots of the waste floating in the amnionic fluid — skin cells, lanugo, mucus, poop, water, and amniotic fluid itself. \
                It's stuff your baby ingests while waiting around to be birthed. Luckily, it is normally held in the intestines of your baby until after birth, and not circulating in the fluid for your baby to snack on.";

                this.usermomtext = "Mom-to-be may have pointed out to you(and you failed to notice), that her nails are becoming harder and hair becoming thicker.\
                She might also be sprouting hair in places shes not used to, such as her belly. This is normal and happens to everyone, most likely to her distate.\
                She may be having more trouble sleeping at this point, and many women end up kicking dad to the couch near the end of pregnancy so that they can fidget and move around in their sleep.\
                Her body is contnuing to grow for the baby thats on its way, and remember to keep feeding her for two.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//19
else if (this.userProfile.dueDate < this.Toneweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Nineteen";
                this.firstuserlefttext = "Once your baby arrives, you and your partner won't have as much time for each other.\
                That's why a babymoon – one last hurrah for just the two of you – is such a great idea. The second trimester is an ideal time to take a trip.\
                By your third trimester you may feel too tired and achy to hit the road. If you can't get out of town, plan local activities you can enjoy together. Even dinner and a movie counts!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'http://d3n00i6iiwtvb3.cloudfront.net/cdn/978470/media/wysiwyg/805289653653_shad_fr_clubmaster.jpg';
                this.userbabyimgtext = "Your baby is the size of a pair of sunglasses!";

                this.whatsnewusertext = "Your baby is now measuring 6.0 inches (15.3 cm) from crown to rump and weighs 8.5 oz (240 grams).\
                our baby's been floating in amniotic fluid for a long time now.\
                All the while your baby's been shedding lanugo, that soft black hair on the body, and dead skin cells.\
                Mixed together, it's called sebum. Where does all the sebum go? In the amniotic fluid.\
                And thats not all, baby's oil glands are also producing a waxy substance squalene.\
                When you combine sebum and squalene, you get this thing called vernix caseosa.\
                It coats your baby's skin from head to toe, essentially acting as a force field to protect your baby from getting pruny.\
                Think about how wrinkly you get sitting 15 minutes in a tub. Imagine what it would be like for a baby floating several months in amniotic fluid without vernix caseosa.\
                It's also believed that the waxy substance acts as a protective layer to keep bacteria out.\
                It's like your baby produces their own baby forcefield.";

                this.usermomtext = "Not a lot has changed for mom this week, or will for the next couple weeks. Except, and this might shock you, but she should be getting bigger\
                Around this time, a lot of pregnant women can complain of leg cramping, which can make it hard to sleep, or similar back pains. Hot or cold pads can be helpful with this, or some easy exercise.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//18
else if (this.userProfile.dueDate < this.Ttwoweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Eighteen";
                this.firstuserlefttext = "Ensuring you have sorted out your finances and work life before your baby arrives will be one big worry off your mind. If you haven’t already told your manager at work, now is a good time, especially if you are thinking of asking about more flexible working arrangements.\
                Some dads now take over as chief carer, allowing mum to return to work, or parents may share the load by adopting flexible working patterns.Your employer has to seriously consider an application for flexible working and can only reject it on business grounds.\
                Different employers may have more or less generous set ups in place for dads, but legally you are entitled to two weeks paternity pay.This is applicable to the baby’s biological father, the husband or the partner, including a same sex partner providing they expect to have responsibility for bringing up the chi";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://images-na.ssl-images-amazon.com/images/I/41Ok4JqJ20L._SY300_.jpg';
                this.userbabyimgtext = "Your baby is the size of a Leatherman Multi-tool!";

                this.whatsnewusertext = "Your baby is now measuring 5.6 inches (14.2 cm) from crown to rump and weighs 6.7 oz (190 grams).\
                Your baby's heart now beats twice as fast as yours does, that's about 140-150 beats per minute.\
                Interestingly, regardless of the sex of your baby, your baby has mammary glands now. For males, they won't develop much further in life.\
                Don't worry too much about him having moobs. Unless it runs in the family.\
                If you haven't found out already, That is if your baby is male you'll be able to see the male parts.\
                It's big enough to see now. Regardless of dad.";

                this.usermomtext = "What's happening with mom's body this week? It's getting bigger.\
                Her uterus which is lengthening and getting more round in shape,\
                so much so that it is pushing aside other organs to make room for itself. Your intestines are getting pushed up and outwards.\
                Eventually they'll shift to the side to make way for a baby to come out. She can most likely feel the baby dancing around now, and may be able to feel the baby hiccuping in there, while he/she practices sucking their thumb. ";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//17
else if (this.userProfile.dueDate < this.Tthreeweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Seventeen";
                this.firstuserlefttext = "If you haven’t already done so, now is the time to get enrolled on antenatal classes. Your local midwife or hospital will run these and they are usually free.\
                NCT also runs antenatal courses in most areas. See if there are any classes in your area.\
                Even if you are already a dad, or have been reading about pregnancy and labour, classes give you the chance to refresh your memory,\
                and ask about things that you don’t understand.Classes will cover topics in more detail and provide more practical preparation for labour and parenthood.\
                They will also look more closely at feelings about pregnancy and birth, preparing for life with a new baby and birthing positions.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'http://www.mcdonalds.co.uk/content/dam/mcdonaldsuk/item/live/mcdonalds-Fries-Medium.png';
                this.userbabyimgtext = "Your baby is the size of a large order of fries!";

                this.whatsnewusertext = "This week your baby is 5.1 inches(13 centimeters), and weighs 4.9 ounces(140 grams).\
                The tips of your baby's fingers are a little less smooth now with the formation of fingerprints.\
                These will continue to form until about the end of the second trimester.\
                Along with this, tiny bones in the ear canal of your baby are starting to develop. These are the bones that translate ear drum vibrations for our brain to process. What this means is that your baby may be able to hear the sound of your voice.\
                Your baby may not understand what you're saying, but any time you talk, your baby's listening.\
                Studies have shown that it helps to talk to the babies often, and they will recgonize moms, and sometimes Dad's, voice outside of the womb.";

                this.usermomtext = "With our mom-to-be,Hormones and milk-producing glands are developing to prepare for nursing.\
                All of this activity, plus an increase in blood flow, can boost breast size up to three cups!\
                Enjoy it while you can, because in a few short months those are going to be off-limits for you.\
                Some heartburn and other symptoms can be common at this stage, if she is having any problems be sure to talk to the doctor about them, but try not to worry too much. ";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//16
else if (this.userProfile.dueDate < this.Tfourweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Sixteen";
                this.firstuserlefttext = "This is a big week for a lot of new parents, as this is the first week where its possible to find out the gender!\
                Most pregnant women find out their baby's sex (if they choose to know) during their mid-pregnancy ultrasound, usually between 16 and 20 weeks.\
                Some people find out their baby's sex through noninvasive prenatal testing (NIPT). This is a blood test that can detect Down syndrome and a few other chromosomal conditions at 10 weeks of pregnancy or later. It also looks for pieces of the male sex chromosome in the expectant mother's blood to see if she's carrying a boy or a girl.\
                Other people find out their baby's sex from a genetic test like CVS or amniocentesis. These tests are usually used to determine whether a baby has a genetic disorder or a chromosomal abnormality like Down syndrome but may carry a slight risk of miscarriage. CVS is typically done between 10 and 13 weeks and amniocentesis between 16 and 20 weeks.\
                Whatever you choose to do, let us know by choosing a gender, or picking suprise if you're going to wait it out!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://newinvestmentclub.files.wordpress.com/2015/07/baby-drinking-coke.jpg';
                this.userbabyimgtext = "Your baby is the size of a can of soda!";

                this.whatsnewusertext = "Your baby now measures 4.6 inches (11.6 centimeters) and weighs 3.5 ounces!\
                Babies skin has begun to develop, although its so clear you can see through it like an x-ray at this point.\
                The babies eyelashes and eyebrows are beginning to grow out, as well as their toenails.The cartilidge in your baby has begun to transform into bone, which is important before their muscles begin developing.\
                For some, this is the first week where the babies kicks can be felt, but it may be too early for you to feel it as well, Dad.";

                this.usermomtext = "Mom-to-be's umbilical cord has begun to develop and grow thicker to support your baby as it grows.\
                Inside of the cord, two arteries are pumping blood to your baby for circulation, and a vein carries deoxygenated blood back to mom-to-bes body for a fill up.\
                Theres also a jelly like substance coating the umbilical cord, to keep it slippery so it doesnt get stuck as the baby moves around inside her belly.\
                ";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//15
else if (this.userProfile.dueDate < this.Tfiveweek) {
                this.userleftstate = 'Second Trimester';
                this.secondTrimester();
                this.userleftheader = "Week Fifteen";
           
                this.firstuserlefttext = "While you are probably more excited about the pregnancy now, you may also find that as the reality of fatherhood looms you have some jitters.\
                Having different emotions about becoming a dad is a common experience and one that you should not keep to yourself\
                Find a friend that has gone through pregnancy, even if its your own Dad, and tell them some of your thoughts you havent mentioned to mom-to-be\
                You will most likely be suprised to find out that everyone has felt the same way. Becoming a parent is a scary thing for anyone!\
                Next week is a big week, as at week 16 you can find out the gender of the baby!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://www.chowhound.com/blog-media/2014/12/sandhiracinnamon-roll13859833365480.jpg';
                this.userbabyimgtext = "Your baby is the size of a warm cinnamon roll!";

                this.whatsnewusertext = "This week your little babys noggin has progressed rapidly.\
                His or her eyes have now reached the point where they can detect light, and might respond to it!\
                The little stump that will become a nose has small holes now, allowing him to breathe into his little air sacs, that will become lungs!\
                Your little baby has also finished developing their mouth, and can now suck on their thumbs, readying themselves for breast feeding.\
                This week, your baby measures 4.0 inches (10.1 centimeters) from crown to rump and weighs 2.5 ounces (70 grams).";

                this.usermomtext = "This week your mom-to-be's bump might be showing even more!\
                Colostrum, a protein-rich fluid that will be your baby's first food when born, if breastfed, is starting to be produced by her body.\
                The milk ducts are also preparing for the first drops of milk. If your mom-to-be is feeling what seems to be contractions, don't fear, these are most likely Braxton-Hicks, which is basically her body practicing for real contractions.\
                Her appetite might be increasing, so be sure to help her fufill those odd pregnancy food cravings.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }
           //14
else if (this.userProfile.dueDate < this.Tsixweek) {
                this.userleftstate = 'Second Trimester';
                this.firstTrimester();
                this.userleftheader = "Week Fourteen";
                
         
                this.firstuserlefttext = "Now that the two of you are most likely finally settling into pregnancy a little bit, you may begin to notice how the routines you have taken for granted are changing.\
                Couples often share basic routines around meals, leisure time and household chores. \
                A simple habit, like a Sunday morning breakfast may no longer be desirable to your partner if she has morning sickness, or is beginning to have aches and sores and wants to take it easy.\
                If you both use to enjoy going out weekend evenings and now she feels too fatigued, how are you going to respond? \
                Try and be understanding with each other about all the happiness, guilt, anticipation, ambivalence and frustration that is now going on in your lives.\
                Establishing a pattern of “open communication” with your partner at this time in the pregnancy will allow the both of you to move toward the birth in a more supportive and positive relationship.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://www.elitespecialtymeats.org/image/data/sirloin-filet-v1.png';
                this.userbabyimgtext = "Your baby is now the size of an 8oz filet mignon!(But a lot more expensive)";

                this.whatsnewusertext = "This week your baby has hit 3.4 inches (8.7 centimeters) long and weighs 1.5 ounces (43 grams).\
                If your little baby is a male, his prostate has just begun development! They have also started a grow a soft coat of hair over their body, to protect their body while its in the womb.\
                The little tiny baby liver has begun to function and produce bile(you're going to be very familar with it eventually.)\
                His/Her red and white blood cells are now being produced, and now they can pee! Right into the same liquid they breathe and drink in! Nature is neat, right?";

                this.usermomtext = "The top of mom-to-be's utereus should now have moved to slightly above her pelvic bone, which can start to show off that baby bump. Mom-to-be should be beginning to get back some of the energy she lost during that first trimester.\
                Morning sickness should also be fading, although some moms report having morning sickness through the entire pregnancy. This week Mom to be might be beginning to feel round ligament pains, which are not a cause for alarm, it is merely her uterus streching to prepare for a baby!";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//13
else if (this.userProfile.dueDate < this.Tsevenweek) {
                this.userleftstate = 'Second Trimester';
                this.firstTrimester();
                this.userleftheader = "Week Thirteen";
                this.firstuserlefttext = "You've made it over the first hill, you're now in the Second Trimester!\
                After the initial excitement and flurry of activity during the first trimester, the second trimester of a pregnancy seems to slip by without much strife, so enjoy it while you can!\
                The Second trimester will bring a lot of changes with it, as your mom- to - be goes through more changes to get ready to birth your little baby!\
                Many expectant dads report that during the second trimester there is a change in the sexuality with their partners.\
                Each expectant mom will respond differently to the hormonal changes her body is going through, and some of them may benefit Dad a little bit.\
                For some women it pregnancy can intensify their sexual arousal, while for others it appears to diminish it.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://i.ytimg.com/vi/dVoeABqDJDc/maxresdefault.jpg';
                this.userbabyimgtext = "Your baby is now the size of a whole pea pod!";

                this.whatsnewusertext = "By now, you've most likely had your first ultrasound of your baby.\
                You've heard the heartbeat and you've seen your baby on screen.\
                As their facial features have started to form, you've probably had a chance to see their little face too.\
                It's too early to tell the sex of the baby, them baby parts are still too small regardless of who dad is, but you'll know (if you choose to) at your next ultrasound.\
               And remember those ovaries your potential female baby were developing back in week 11? Well they're now full of eggs. Lots of them. About 7 million right now. And remember those not ovaries your male baby were developing back in week 11? Nothing's changed.\
                What has changed regardless of your baby's sex is the size of their torso. It's grown to the point where it is now 2/ 3 of your baby's overall length.";

                this.usermomtext = "All the morning sickness should be improving now as the placenta has taken control of the good ship hormones and has started producing them instead of your own body.\
                Energy will begin to increase and your tiredness will decrease, but dont let her get too used to it.\
                She probably isn't quite showing yet, but its coming!\
                Even though your baby is still only about 2.9 inches (7.4 centimeters) big and weighs only 0.81 ounces (23 grams), the uterus has continued to grow to make room ahead of the baby's growth.\
                At this point it's size is such that it can no longer be contained in her body cavity, and begin to form her baby bump.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
               this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }
/*-----------------------------FIRST TRIMESTER HERE--------------------------------------------------------------------- -------------------*/
                //week 12
else if (this.userProfile.dueDate < this.Teightweek) {
                this.userleftstate = 'First Trimester';
                this.firstTrimester();
                this.userleftheader = "Week Twelve";
                this.firstuserlefttext = "Week Twelve is the final week in the first trimester! Congrats!\
                It is also marked by many as the time to start telling people of the pregnancy, as the risks of miscarriage are much lower from this point forward.\
                This is also a great time to begin setting up registries at various places, people are going to want to know what you want for your baby!\
                As we head into the Second Trimester remember to keep up on your weekly updates, and more importantly, keep up with your mom-to-be and how shes feeling.";
                this.userbabyimg = 'https://pbs.twimg.com/profile_images/754479423223570432/yWfHOCgc_400x400.jpg';
                this.userbabyimgtext = "Your baby is now the size of a chicken nugget!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.whatsnewusertext = "Right about this time, your baby's internal organs have all begun their development and are functioning at a rudimentary level. Your babies eyes are slowly moving from the side of the head to their rightful place on the front of the face.\
                With each passing day, your baby's muscles keep getting stronger, while the limbs continue to grow. If your little monster is a female, shes beginning to develop ovaries!\
                The next stage of their development is to grow larger and practise sucking and breathing motions.. It’s likely you’ll see your baby for the first time this week at your dating scan, though it’s unlikely you will be able to get a reliable sexing just yet.\
                They are moving around a lot at this stage, but mom-to-be and you  still wont be able to feel this for a while as they still have a lot of space to bob about in.\
                Their eyelids remain fused shut at week 12 (they will open at around week 27) while their little wrists and elbows bend as they move around.\
                At 12 weeks pregnant your baby’s skeleton is currently made of cartilage (like you’d find in your nose or ear), but over the coming weeks this will harden into bone.\
                When your baby is born their skeleton is made up of over 300 parts of bone and cartilage, but they will only have 206 as an adult, as bones fuse together to form larger, stronger bones.";

                this.usermomtext = "Mom-to-bes body is going through lots of changes still as it continues to adjust to the pregnancy.\
                Her gastrointestinal muscles have begun to slow down, which may mean tougher poops and more gas. At least now you can compete on even ground with your partner in unannounced flatulence.\
                As progesterone keeps being generated by the pregnant body, her other muscles such as the throat and stomach muscles have also loosened which could mean increased heartburn or acid reflux.\
                The good news though is that any nausea that she has been feeling for the last several weeks will begin to subside.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//week 11
else if (this.userProfile.dueDate < this.Tnineweek) {
                //set dates
                this.userleftstate = 'First Trimester';
                this.firstTrimester();
                this.userleftheader = "Week Eleven ";
                this.firstuserlefttext = "By week 11 you probably have an appointment lined up for your dating scan – the most exciting part of the first trimester – seeing your baby on the screen for the first time and getting your due date\
                 You probably watched as Mom-to-be was submitted to a series of tests, thanking god for the millionith time this pregnancy that you were born a boy as you watched them hit her with a ton of needles(among other less comfortable things)\
                You might be thinking about breaking the news to work colleagues and your wider family and friends around now, and now is a great time! As you enter the second trimester the risks towards your new baby greatly decrease. Your first ultrasound scan could also identify if you are carrying more than one baby. \
                The “is it twins?” thought crosses the mind of every expectant parent and for one in every 65 couples the thought becomes a reality at the first scan.\
                But earlier signs may also raise your suspicions that there may be twins growing in there.\
                Signs such as a faster growing bump in the early weeks and experiencing extreme tiredness or virtual exhaustion can also be a sure fire sign there are two babies zapping both of your energy";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS-RvhsJ-IEvxczKuJ3mTQ9JSLVmrwhlx9bufwi8Zw1Q36WuqpWqg';
                this.userbabyimgtext = "Your baby is the size of a golf ball!";

                this.whatsnewusertext = "At week 11 of pregnancy, your baby's arms and legs begin to function, gracefully he will soon be sweeping across your stomach like a dancer.\
                Or not.\
                Your baby is now approximately 1.6 inches (4.1 centimeters) big and weighs 0.25 ounces (7.1 grams).\
                It is still be a while yet before your baby is big enough and the kicks strong enough for you to feel them.\
                But many other developments begin this week\
                The elbow, knee, shoulder and hip joints are beginning to take shape.\
                Fingernails are starting to grow on each of your baby's tiny little fingers. Toenails too.\
                Peach fuzz hair is sprouting form the top of your baby's noggin, if its a hairy baby.\
                Underneath your baby's gums, teeth are beginning to form. I wouldn't be worried about these growing out any time soon.\
                Nipples.Yes, nipples.\
                If your baby is a girl, ovaries.If your baby is a boy, not ovaries.";

                this.usermomtext = "Mom-to-be is probably still struggling with the same symptons she has been, but theres a light at the end of the tunnel soon: THE SECOND TRIMESTER\
                Stuff she might be dealing with this week, which probably isn't much different from the previous weeks\
                Round ligament pains and muscle aches around the hips may be increasing as her body continues to prepare itself for pregnancy.\
                Veins that are close to the surface of her body may start to become blue and visible depending on her skin tone.Mom-to-be's arteries are growing too to accommodate more blood for transfer to and from the placenta.\
                    Nausea.Seriously, when will it all end? I hope you tried my ginger ale suggestion.\
                        Tiredness, constant peeing, Poor sleep because you need to pee.\
                Need to pee causing poor sleep.\
                Belly growth.Hopefully this is what you've been waiting for! Either way hold on for mom-to-be and keep taking those grocery store craving trips, things will be easier soon.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);                
                this.slides = [

                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//10
else if (this.userProfile.dueDate < this.thirtyweek) {
                //set dates
                this.userleftstate = 'First Trimester';
                this.firstTrimester();
                this.userleftheader = "Week Ten ";
                this.firstuserlefttext = "You are now ten weeks into your pregnancy and things are really coasting along!\
                The most critical stage of your babies development is now complete, and they are starting to enter the ‘foetal period’ where tissues and organs grow rapidly and begin to mature.\
                Now is the perfect time for you and your mom-to-be to start making to-do lists and choosing priorities! Do you need to move into a new home? Are you going to breastfeed? Are you going to co-sleep?\
                There are an almost infinite amount of things to prepare for in pregnancy, but dont let it all overwhelm. Tackle things one at a time and remember you still have 7 months left!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Ferrero_Rocher.png/150px-Ferrero_Rocher.png';
                this.userbabyimgtext = "Your baby is now the size of a Ferrero Rocher!";

                this.whatsnewusertext = " Your baby has officially graduated this week from embryo to fetus, and with that change are a whole bunch of others happening in its development.\
                The outside part of your baby's ear is beginning to look more like an ear. The inner ear is also forming and looks more like... the inside of an ear. It will be still be a little while before your baby can hear your voice.\
                But the ears aren't the only thing on your baby's head that is developing. This week, facial features are starting to take shape. Mom's nose, father's eyes, these and other facial traits that were decided back in week 3 are becoming easier to recognize.\
                Your baby's brain is now plugged in and online! It has connected itself to the spinal cord and has begun sending signals throughout the body.\
                This week, your baby is 1.2 inches (3.1 centimeters) in length and 0.14 ounces (4.0 grams) in weight\
                At this point in the pregnancy, if you have a handheld fetal doppler monitor,you may be able to hear the sound of your baby's heartbeat. Doppler monitors are a safe and fun way to connect with your baby without having to go to the obstetrician just to hear the sound of your baby.";

                this.usermomtext = "Some mothers may already be starting to show at this week.\
                A lot of whether or not she is showing at this point in your pregnancy depends on a combination of her body frame, whether this is her first pregnancy and genetics. If you are starting to show though, you dont have quite the same excuse.\
                 Now is a good time to begin mom-to-be on prenatal vitamins(after talking to your doctor), as her morning sickness may be beginning to wear away. Now is a great time for you and mom-to-be to go on some hikes, and try to stay active throughout your pregnancy.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//9
else if (this.userProfile.dueDate < this.toneweek) {
                //set dates
                this.userleftstate = 'First Trimester';
                this.firstTrimester();
                this.userleftheader = "Week Nine ";
                this.firstuserlefttext = "As the end of the first Trimester looms, you might want to start thinking about whether or not you two will find out the gender! \
                Although an exciting thing to find out early, many parents choose to wait for the suprise at the end.\
                For what it's worth, the morning sickness will likely go away completely within another 3 weeks or so. Many women report the second trimester being so much easier than the first. (And finally being able to tell her friends that she's pregnant and get their help and support will make it easier, too!)\
                You doing all the chores is going to be normal for a while; while the pregnancy should overall get easier, her physical disability will increase in some(obvious and not- obvious) ways over time, so stay tough out there.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'http://www.seriouseats.com/recipes/assets_c/2011/03/20110325-slideshow-food-lab-tater-tots-primary-thumb-625xauto-149109.jpg';
                this.userbabyimgtext = "Your baby is now the size of a Tater Tot!";

                this.whatsnewusertext = "Your baby is starting to look more human-like each day! What a relief! Your baby is now bigger too, at 0.91 inches (2.3 centimeters) and 0.071 ounces (2.0 grams).\
                Fingers aren't nearly as webbed as they were before, and neither are the toes. At this point, it should be possible to distinctively count the little piggies.\
                Joints like knees and elbows, wrists and ankles begin to form. This makes it much easier for your baby to do the robot.\All of your baby's major internal organs are now developing. They aren't all functioning yet, but they are rapidly taking shape.";

                this.usermomtext = "You should have noticed a change in your mom-to-be's weight since shes probably weighing yourself quite often. Some women gain weight, some lose weight. It's normal either way, and different for everyone.\
                All those first trimester symptoms mentioned in week 8 may still kicking her butt. Never fear, it gets better about week 12, although this has been a cakewalk so far for us dads, right? \
                ...............right?.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//8
else if (this.userProfile.dueDate < this.ttwoweek) {
                //set dates
                this.userleftstate = 'First Trimester';
                this.firstTrimester();
                this.userleftheader = "Week Eight ";
                this.firstuserlefttext = "Pregnant women often need to alter their diet and exercise habits, so it might benefit you to change yours with her and show some support(and maybe even get a little healthier)\
                Don’t stop asking questions – mainly, 'What can I do to help? Right now, she is in the most vulnerable state--physically and mentally. She needs you. She needs to know that no matter how crazy she acts, you will be calm and patient and strong and there for her. You are now the father of her child, and her dependency on you is at a high point right now.\
                At this point, the pregnancy is taking its toll on your mom-to-be, and shes not probably not feeling too well either. Now is the time to be a strong support system, whether that be laundry,\
                cooking dinner, or just watching her newest episode of bachelorette with her. (I did it, you can to).";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'http://img.wennermedia.com/social/mj-618_348_five-rules-for-a-healthier-halloween-stay-full-with-peanut-m-ms.jpg';
                this.userbabyimgtext = "Your baby is now the size of a peanut m&m! ";

                this.whatsnewusertext = "Your baby has a huge growth spurt in week 8 of pregnancy and has doubled in size since the previous week. In fact, at 0.63 inches (1.6 centimeters), they are now 20 times bigger than they were in week 3 and now weigh 0.039 ounces (1.1 grams) now.\
                Your baby's hands and feet are still webbed, but are slowly forming to become fingers and toes. Your baby's arms are also becoming longer and now overall looks a lot less like a baby T- Rex.\
                The spinal cord has been growing fast, so much so that it looks like a little tail. No worries though, as this week the body catches up in size and the tail disappears.\
                The first lung cells have begun to develop, but they won't be fully functional until birth.";

                this.usermomtext = "Mom has a lot of symptons beginning as we head closer to the second trimester. Bigger boobs - Just when you thought they couldn't get any bigger, they do! Milk ducts are starting to prepare themselves for milk production. Enjoy it while it lasts, for you shall soon surrender them to your child.\
                Super smelling powers - Like Spidey senses, but worse, because she can now smell that guy with the bad BO from further away\
                Stretchy ligaments - The ligaments in mom-to-bes hips are starting to lengthen out so that your hip bones can expand for your baby's arrival.\
                Sleepiness - Always, constantly, tired. Pro Tip: this isn't going to change anytime soon\
                Dreams - Now more vivid and probably involving some infants.\
                Constant need to pee - Mom-to-be is making more trips to the bathroom than you ever thought possible, and once again, this one doesnt change anytime soon.\
                And the constand food battle, although by now you two have probably started to figure out some of those cravings.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//7
else if (this.userProfile.dueDate < this.tthreeweek) {
                //set dates
                this.userleftstate = 'First Trimester';
                this.firstTrimester();
                this.userleftheader = "Week Seven ";

                this.userbabyimgtext = 'Your baby is now the size of a tic-tac!';
                this.userbabyimg = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQUXoum90i_vf1Zn059n5adQbmxE6iEO6hWpdYveTtvl9IsG2Bz';
                //text variables

                this.firstuserlefttext = "Welcome the the seventh week! Now is the time to make sure you get into a doctor and start getting your little baby checked up on. Your partner may not have told anyone at work that she's pregnant. The pressure to carry on as normal, when she feels tired, emotional and sick, means she will need your support at home.You do have a legal right to take unpaid time off if you want to go to the antenatal appointment with her.\
                However, you're only entitled to time off for up to two appointments. If your employer won't let you take extra time off, you may prefer to wait until the ultrasound scans.\
                You might be struggling a little bit with all the changes, and watching your mom-to-be suffer through the first trimester symptoms. This might be rough on you, but your a Dad now.  Keep your priorities straight and realize it's going to be a rough couple of years.\
               Continue to love her, continue to not complain. Continue to take on tasks to ease her hellish existence.\
                It sucks, but you are awesome for doing it and the reward you get it an appreciative mom-to-bes (when she's back to normal) and a tiny person who will adore you simply for existing";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.whatsnewusertext = "This week the babys brain has begun developing! Along with this, the umbilical cord and placenta have formed and attached itself to you. Your baby will be feeding through it soon.\
                The baby now has a heart, just like the Tin Man at the end of Wizard of Oz. Your baby's heart is now beating over 140 beats per minute and pumps blood through newly created blood vessels.\
                The liver and kidneys are now open for business, getting ready for that ever important first bowel movement in 7 and a half long months. Finally, Those flipper appendages that your baby was growing are becoming longer, and the ends are beginning to form webbed fingers and toes.\
                This week, your baby measures about 0.31 inches, or for pretty much the rest of the world, 0.79 centimetres. Weight is now around 0.018 ounces (0.50 grams).";
                

                this.usermomtext = "This week you may have noticed the nausea really start to kick in. At 7 weeks pregnant, crackers are her life right now or, at least, the mainstay of her rapidly dwindling diet.\
                 Their comforting starchiness can take the edge off her nausea so she can better confront breakfast.\
                 Word to the wise: Encourage the habit — and bring her a glass of ginger ale to wash them down.\
                 You might also notice that she's having aversions to foods that used to be favorites. Is she really passing on the pizza?\
                 Whatever cravings she is having, dont be afraid to fufill them, the food she takes in at this point doesn't matter too much as far as the baby is concenered. .";

                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                //tip slides
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }//week 6
else if (this.userProfile.dueDate < this.tfourweek) {
                //set dates
                this.userleftstate ='First Trimester';
                this.firstTrimester();
                this.userleftheader = "Week Six ";
                this.userbabyimgtext = 'Your baby is the size of a sweet pea. The average embryo at week six is about .25 inches and will double in size again next week!';
                this.userbabyimg = 'https://i1.wp.com/bonnieplants.com/wp-content/uploads/2011/10/snap-pea-pod.jpg?ssl=1';
                //text variables
                this.firstuserlefttext = "At 6 weeks, you’re one month and about one week pregnant—even though it’s probably been only a week or two (or even less) since you found out you and your mom-to-be were expecting.\
                Because its early in the journey, theres not a whole lot going on for you to take care of yet, Dad. Your mom-to-be is probably beginning to experience some morning-sickness, and some of the anxiety that tends to accompany pregnant woman.\
                Just remember you're there to be sane and support her, you're not about to grow a literal human inside you!\
                This week is a great time to make your first pre-natal appointment, and maybe check in with your mom-to-be to make sure everythings going well for her.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.whatsnewusertext = "Your little one has tripled in size and is protected by amniotic fluid, but he is still only about an 1/8 of an inch long. Many of his crucial development areas are starting now.\
                 He's currently weighing in for the fight at around 0.04oz. It may seem small but he still packs a punch, just ask moms nausea. Baby is circulating blood with an increasingly sophisticated circulatory system.\
                Baby might even be wiggling his or her paddle-like hands and feet. Your 6-week embryo is about to get cuter too, since he or she is starting to sprout a nose, eyes, ears, chin, and cheeks.\
                ";

                this.usermomtext = "This week the symptom of pregnancy that affects three of four women might begin to appear: Morning Sickness\
                    Hate to break it to all the dads-to-be, but morning sickness doesn't just happen in the morning.\
                    It can be an all-day affair. And moms-to-be who are six weeks pregnant with twins might have even more severe nausea.\
                    It’s a good idea to find foods that help settle her stomach and to keep them on hand for regular snacking, since having an empty stomach can trigger bouts of nausea.\
                    She might also appreciate you staying away or being a little more sensitive during 'play time', as her breasts are probably beginning to feel sore and prep themselves for breastfeeding, even this early.";

                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                //tip slides
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "s" }
                ];
            }//week 5
else if (this.userProfile.dueDate < this.tfiveweek) {             
                this.userleftstate ='First Trimester';
                this.firstTrimester();
                this.userleftheader = "Week Five ";             
                this.firstuserlefttext = "You may be overjoyed and repressing a craving to shout from the rooftops about that positive home pregnancy test. It's not surprising that you're eager to alert your family and friends, especially if this is your first pregnancy — but how soon is too soon?\
                Some couples can't wait to tell their friends they're pregnant (if they could, they'd shout it from the highest mountain — or make the announcement go viral)\
                Others prefer to wait till the three month mark, when the risks of miscarriage greatly decrease.\
                There's no right or wrong time to tell or way to tell, do whatever works for you. Tell now, tell later, tell some people, tell everyone, do whatever works for you.\
                So talk it over with your mom-to-be, and do whatever feels natural.\
                Just be ready for some of the advice you might not be ready to hear yet!";
                this.userbabyimg = 'https://femininealchemy.files.wordpress.com/2016/01/orange-seed.jpg';
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimgtext = 'Your baby is about the size of a Orange Seed Right now!';

                this.firstTriText = "Food Cravings & Aversions\
               Has mom-to-be had an urge to eat a grilled cheese sandwich? She can’t stand the sight of salads (or anything green) ? Welcome to the wacky world of food cravings and aversions.\
                Hormones play a role here (as they do in most pregnancy symptoms) — especially in these early weeks when her body is getting used to hormonal havoc.If you can’t beat ’em, join ’em — within reason.\
                Don't be afraid to make that run to the store for her, or to buy two of those candy bars.";

                this.whatsnewusertext = "At five weeks gestation, they will be around 5mm long, and their heart, brain and spine will be developing at a  lightning pace.\
                Soon, your baby's heart will actually start beating, and if you have an early scan in the coming weeks, you would see it flickering on the monitor.\
                Your baby’s head is also seeing signs of development with the neural tube developing (the start of the connection between their spinal cord and brain) which will help to regulate your baby’s vital functions along with heart rate and blood supply.\
                Whilst your baby might currently resemble a tiny tadpole, their limbs are also starting to develop with nubs starting to turn into small limp flippers, the precursor to arms and legs forming.";

                this.usermomtext = "By now, mom-to-be should have missed that first period, one of the big signs that a little bean is growing in her belly.\
                 More then likely, she is also now experiencing some exahustion, and possibly morning sickness. Even when your baby is still tiny in there, her body is working hard to prepare for the hard eight months ahead.\
                  Inside her body Large quantities of hormones — chemical signals that circulate in your body and work together to cause physical changes — are being mass-produced this week.\
                  ";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                //tip slides
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
                
            }//4
else if (this.userProfile.dueDate < this.tsixweek) {
                
                this.userleftstate = 'First Trimester';
                this.firstTrimester();
                this.userleftheader = "Week Four ";
                //text variables
                this.firstuserlefttext = "Everyone is talking about the changes the mother’s body will go through,but less is said about the changes your home will undergo.\
                There will be a lot more “stuff” to fit into what was once your private castle: strollers, high-chairs, and of course the baby will need somewhere to sleep.\
                If there needs to be any redecorating, guess who is responsible for doing it?\
                 But!don’t get so hung up on the house that you forget that your partner is actually going through a lot. Many doctors explain that while the external changes are minor, a woman’s body is involved in a great deal of unseen work in the first trimester.\
                She will frequently be very tired, even if she is sleeping well. It’s normal, and a good partner should make allowances for this by providing extra help where possible so she can rest.";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);           
                this.userbabyimg = 'http://www.luminescents.net/wp-content/uploads/2014/06/Aniseed-whole-copyright-d-hugonin-220x220.jpg';

                this.userbabyimgtext = 'Your baby is about the size of a Poppy Seed Right now!';

                this.whatsnewusertext = "Currently your baby just a tiny ball (called a blastocyst), consisting of several hundred cells that are multiplying madly.\
                The part of it that will develop into the placenta has started producing the pregnancy hormone hCG (human chorionic gonadotropin), which tells your ovaries to stop releasing eggs and triggers increased production of estrogen and progesterone.\
                These hormones keep your uterus from shedding its lining – and its tiny passenger – and stimulate the growth of the placenta.\
                Meanwhile, amniotic fluid is beginning to collect around the ball of cells in the cavity that will become the amniotic sac.This fluid will cushion your baby in the weeks and months ahead.\
                Right now, your little blastocyst is receiving oxygen and nutrients (and discarding waste) through a primitive circulation system made up of microscopic tunnels that connect your developing baby to the blood vessels in your uterine wall.\
                The placenta won't be developed enough to take over this task until the end of next week. ";

                this.usermomtext = "Mom has a lot of symptoms beggining this week, lets go through some of them. Bloating:she may be a little puffed up thanks to the pregnancy hormone progesterone.\
                Mild cramping:At 4 weeks pregnant cramping might worry both of you, but it actually may be a sign that baby has properly implanted in the wall of her uterus.However, any severe cramping or pain at 4 weeks pregnant is something you should definitely tell your doctor about right away.He or she will want to examine Mom-to-be to rule out any problems.\
                Spotting: Light bleeding can also occur during week 4 as a result of implantation.Don't worry—this is totally normal too. But the same advice goes: If it’s a lot of blood,get mom-to-be to the doctor.\
                Mood swings: It’s not your imagination, she is more hormonal then normal. Learn her moods and when its sometimes better to leave her alone. \
                Morning sickness: Experts say that about 50 to 90 percent of pregnant women get some form of morning sickness (a.k.a.nausea and sometimes vomiting too).So even if she hasn't had an upset stomach yet, she probably will at some point.Morning sickness is usually at its worst around nine weeks and then slowly gets better, typically disappearing completely in the second trimester.\
                Fatigue: One of the most common four weeks pregnant symptoms is total exhaustion, as the pregnant body is working hard to grow that teeny ball of cells into an embryo.\
                Sore breasts: Mom-to-be boobs are swollen and tender because of those surging hormones telling her body, “There’s a baby coming.Better start prepping those milk ducts!”";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135); 
                //tip slides
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": " “Will I ever have sex again?” A: It’s up to you." },
                    { "id": 3, "name": "Ginger Ale can be a big help with morning sickness" },
                    { "id": 2, "name": "Prepare yourself for the Naming suggestions" }
                ];
            }
                //week 3
else if (this.userProfile.dueDate < this.tsevenweek) {
                this.userleftstate = 'First Trimester';

                this.firstTrimester();

                this.userleftheader = "Week Three ";
                this.firstuserlefttext = "”Will I become my dad?” ”Afraid of the unknown.” “Didn’t have a clue.” “Worried about every-thing.” “Panicked.”\
                We're sure these are some of the thoughts racing through your head! But no need to worry, we're here to guide you through it week by week.\
                From now until you snip the cord, a lot may happen that no one will have prepared you for ahead of time. \
                The thing about men and pregnancy is that there's only so much you can do -- the expectant mother really does all the work. She also gets all the attention. We all know she deserves it\
 		        -- and then some -- but it's a common source of tension for couples during pregnancy.\
                So along the way we will also keep you up to date with whats going with mom-to-be,\
 		        so you know whats going on inside that growing belly!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.userbabyimg = 'http://www.luminescents.net/wp-content/uploads/2014/06/Aniseed-whole-copyright-d-hugonin-220x220.jpg';
                
                this.userbabyimgtext = 'Your baby is about the size of a Poppy Seed Right now!';
                
                this.whatsnewusertext = "Currently your baby just a tiny ball (called a blastocyst), consisting of several hundred cells that are multiplying madly.\
                The part of it that will develop into the placenta has started producing the pregnancy hormone hCG (human chorionic gonadotropin), which tells your ovaries to stop releasing eggs and triggers increased production of estrogen and progesterone.\
                These hormones keep your uterus from shedding its lining – and its tiny passenger – and stimulate the growth of the placenta.\
                Meanwhile, amniotic fluid is beginning to collect around the ball of cells in the cavity that will become the amniotic sac.This fluid will cushion your baby in the weeks and months ahead.\
                Right now, your little blastocyst is receiving oxygen and nutrients (and discarding waste) through a primitive circulation system made up of microscopic tunnels that connect your developing baby to the blood vessels in your uterine wall.\
                The placenta won't be developed enough to take over this task until the end of next week. ";

                this.usermomtext = "Around the end of this week, you may be able to see a positive result on a home pregnancy test.\
                If the result is negative, don't assume you're not pregnant – you may have just tested too early. Tests are more likely to give an accurate result if you wait a few days to a week after you miss your period. \
                Wait a few days, and if you still haven't gotten your period, test again. If another week or more goes by and you still haven't gotten your period or a positive result, make an appointment to see your doctor or midwife.";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }
                //week 2
else if (this.userProfile.dueDate < this.teightweek) {
                this.userleftstate = 'First Trimester';

                this.firstTrimester();

                this.userleftheader = "Week Two ";
                this.firstuserlefttext = "You've probably just found out the scary, exciting news that you have a baby on the way.\
                You might be scared, but we're here to help as you go!\
                Dont forget to check back over coming weeks and find out whats going on with your baby!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.firstTriText = "First Trimester Text for Week 1";

                this.whatsnewusertext = "Your baby is not quite developing yet! You and your Mom-To-Be not quite pregnant yet, as conception begins two weeks after what would have been the end of your period!";

                this.usermomtext = "Because we're only at Week two, Mom hasn't concieved yet. Although all the materials are in place and the blueprints are drawn up, she will not have officially concieved until two weeks after the end of her period. (Week 3)";


                this.userbabyimgtext = 'Your baby is still too small to measure with the human eye!'
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "Slide for Tips!" },
                    { "id": 1, "name": "Don't forget that at one point YOUR Dad was as scared as you are right now" },
                    { "id": 3, "name": "Ginger can be a big help with morning sickness" },
                    { "id": 2, "name": "" }
                ];
            }
                //week one
else if (this.userProfile.dueDate < this.tnineweek) {
                this.userleftstate = 'First Trimester';

                this.firstTrimester();

                this.userleftheader = "Week One ";

                this.firstuserlefttext = "You might just be testing the waters, but pregnancy doesnt begin till two weeks after your mom-to-be's last cycle ends!";
                this.shortfirstuserlefttext = this.firstuserlefttext.substring(0,135);
                this.firstTriText = "First Trimester Text for Week 1";

                this.whatsnewusertext = "Your baby is not quite here yet, but we hope youre trying!";

                this.usermomtext = "Moms not quite pregnant yet!";

                this.userbabyimgtext = "Your baby is still too small to measure with the human eye!"
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135);
                this.slides = [
                    { "id": 0, "name": "" },
                    { "id": 1, "name": "" },
                    { "id": 3, "name": "" },
                    { "id": 2, "name": "" }
                ];
            }
else if (this.userProfile.dueDate < this.fortyweek) {
                //set dates
                this.userleftstate = 'First Trimester';
                this.firstTrimester();
                this.userleftheader = "Negative Week ";
                //text variables
                this.firstuserlefttext = "Negative Week";
                this.firstTriText = "Negative Week";
                this.whatsnewusertext = "Negative Week";
                this.usermomtext = "Negative Week";
                this.shortwhatsnewusertext= this.whatsnewusertext.substring(0,15);
                this.shortmomtext = this.usermomtext.substring(0,135); 
                //tip slides
                this.slides = [
                    { "id": 0, "name": "Negative Week" },
                    { "id": 1, "name": "Negative Week" },
                    { "id": 3, "name": "Negative Week" },
                    { "id": 2, "name": "Negative Week" }
                ];
            }
else if (this.userProfile.dueDate < this.ftwoweek) {
                //set dates
                this.userleftstate = 'First Trimester';
                this.firstTrimester();
                this.userleftheader = "Negative Week ";
                //text variables
                this.firstuserlefttext = "Negative Week";
                this.firstTriText = "Negative Week";
                this.whatsnewusertext = "Negative Week";
                this.usermomtext = "Negative Week";

                //tip slides
                this.slides = [
                    { "id": 0, "name": "Negative Week" },
                    { "id": 1, "name": "Negative Week" },
                    { "id": 3, "name": "Negative Week" },
                    { "id": 2, "name": "Negative Week" }
                ];
            }
else if (this.userProfile.dueDate < this.fthreeweek) {
                //set dates
                this.userleftstate = 'First Trimester';
                this.firstTrimester();
                this.userleftheader = "Negative Week ";
                //text variables
                this.firstuserlefttext = "Negative Week";
                this.firstTriText = "Negative Week";
                this.whatsnewusertext = "Negative Week";
                this.usermomtext = "Negative Week";
                //tip slides
                this.slides = [
                    { "id": 0, "name": "Negative Week" },
                    { "id": 1, "name": "Negative Week" },
                    { "id": 3, "name": "Negative Week" },
                    { "id": 2, "name": "Negative Week" }
                ];
            }

else {
                this.userleftstate = 'broken';
            }//end of comparison loops
        });
    }//end of ionviewdidenter
}//end of home page Code



       
    


