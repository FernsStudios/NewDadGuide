﻿import { Component } from '@angular/core';
import {  ViewController, NavController} from 'ionic-angular';
/**
 * allows users to add a to do item to the to do list
 */
@Component({
    selector: 'page-addtodo',
    templateUrl: 'addtodo.html',
})
export class AddtodoPage {
    title;
    description;
    constructor(public navCtrl: NavController, public view: ViewController) {}
    saveItem() {
        let newItem = {
            title: this.title,
            description: this.description
        };
        this.view.dismiss(newItem);
    }// Saves a new todo item to the todo array
    close() {
        this.view.dismiss();
    } //goes back to todo page
}