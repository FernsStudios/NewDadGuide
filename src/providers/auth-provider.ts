﻿import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase/app';

@Injectable()

export class AuthProvider {

    constructor(public afAuth: AngularFireAuth) {
    }

    loginUser(newEmail: string, newPassword: string): firebase.Promise<any> {
        return this.afAuth.auth.signInWithEmailAndPassword(newEmail, newPassword);
    }//login function, using firebase .signInWithEmailAndPassword, passing the email and pass put in

    resetPassword(email: string): firebase.Promise<any> {
        return this.afAuth.auth.sendPasswordResetEmail(email);
    }//reset pass function, using firebase built in sendPasswordResetEmail, will automatically allow users to change their password

    logoutUser(): firebase.Promise<void> {
        firebase.database().ref('/userProfile')
            .child(firebase.auth().currentUser.uid).off();

        return firebase.auth().signOut();
    }// log out function, called in the profile page. pushes the current user out, then clears the uid or else the profile page will fuck up 
    //and show the last user(that logged out)s information 

    signupUser(email: string, password: string, dueDate: string, firstName: string, lastName: string, momfirst: string): firebase.Promise<any> {
        return firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(newUser => {
                firebase.database().ref('/userProfile').child(newUser.uid)
                    .set({
                        email: email,
                        dueDate: dueDate,
                        firstName: firstName,
                        lastName: lastName,
                        momfirst: momfirst
                          
                    });
            });
    }///sign up function, uses createUserWithEmailAndPassword from firebase


}//end of AuthProvider
