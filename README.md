## Synopsis

The New Dad Guide
Hybrid Ionic 3 App
## Dependencies 
ionic-angular:3.2.1
firebase:3.9.0
rxjs:5.1.1
zone.js:0.8.10
angularfire2: 4.0.0-rc.0
## Platforms
Android

## Installation

pull the repo
cd into the repo
npm install 
ionic lab 

## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.



## License

A short snippet describing the license (MIT, Apache, etc.)