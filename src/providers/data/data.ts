﻿import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
@Injectable()
export class DataProvider {
    constructor(public storage: Storage) {}

    getData() {
        return this.storage.get('todos');
    }

    save(data) {
        let newData = JSON.stringify(data);
        this.storage.set('todos', newData);
    }
    //this is parsing the todo list, allows them to create and view them
}