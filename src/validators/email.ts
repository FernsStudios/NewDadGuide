﻿import { FormControl } from '@angular/forms';
export class EmailValidator {
static isValid(control: FormControl) {
const re = /^\w+@[a-zA-Z_.]+?\.[a-zA-Z]{2,3}$/.test(control.value); //requires email to have .com, and an @ sign
    if (re) 
    {
    return null;
    }
    return {
    "invalidEmail": true
    };
}
} //this gives you a validate to use when using email forms, makes sure it is a real email