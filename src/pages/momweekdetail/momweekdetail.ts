import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

@Component({
  selector: 'page-momweekdetail',
  templateUrl: 'momweekdetail.html',
})
export class MomweekdetailPage {
userstate;
usertext;
    constructor(public navParams: NavParams) {}
    
    ionViewDidLoad() 
    {
        this.userstate = this.navParams.get('userstate');
        this.usertext = this.navParams.get('usertext');
    }
}
