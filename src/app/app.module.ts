﻿import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { BrowserModule } from '@angular/platform-browser';
import { IonicStorageModule } from '@ionic/storage';
import { AuthProvider } from '../providers/auth-provider';
import { AngularFireAuth } from 'angularfire2/auth';
import { ProfileProvider } from '../providers/profile'  ;
import { Login } from '../pages/login/login';
import { Signup } from '../pages/signup/signup';
import { Resetpassword } from '../pages/resetpassword/resetpassword';
import { Profile } from '../pages/profile/profile';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { IntroPage } from '../pages/intro/intro';
import { TodosPage } from '../pages/todos/todos';
import { AddtodoPage } from '../pages/addtodo/addtodo';
import { TodosDetailPage } from '../pages/todos-detail/todos-detail';
import { DataProvider } from '../providers/data/data';
import { WeekDetailPage } from '../pages/week-detail/week-detail';
import { BabyweekdetailPage} from '../pages/babyweekdetail/babyweekdetail';
import {MomweekdetailPage} from '../pages/momweekdetail/momweekdetail';
import {DoctorpagePage} from '../pages/doctorpage/doctorpage';
import { AdMobFree } from '@ionic-native/admob-free';
import { ExpandableComponent } from '../components/expandable/expandable';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { ConnectivityServiceProvider } from '../providers/connectivity-service/connectivity-service';
import { Geolocation } from '@ionic-native/geolocation';

const firebaseConfig = {
  apiKey: "AIzaSyDGihL3oLfrYxJJ5Jw6UecweH5s_mLydek",
    authDomain: "the-new-dad-guide.firebaseapp.com",
    databaseURL: "https://the-new-dad-guide.firebaseio.com",
    projectId: "the-new-dad-guide",
    storageBucket: "the-new-dad-guide.appspot.com",
    messagingSenderId: "321744267966"
};

const cloudSettings: CloudSettings = {
    'core': {
        'app_id': 'APP_ID'
    }
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Login,
    Signup,
    Resetpassword,
    Profile,
    IntroPage,
    TodosPage,
    AddtodoPage,
    TodosDetailPage, 
    WeekDetailPage, 
    BabyweekdetailPage,
    ExpandableComponent,
    DoctorpagePage,
    MomweekdetailPage               
  ],

  imports: [
    IonicModule.forRoot(MyApp),
    BrowserModule, 
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    CloudModule.forRoot(cloudSettings)
  ],

  bootstrap: [IonicApp],

  entryComponents: [
    MyApp,
    HomePage,
    DoctorpagePage,
    Login,
    Signup,
    Profile,
    Resetpassword,
    IntroPage,
    TodosPage,
    AddtodoPage
    ,TodosDetailPage,
    WeekDetailPage,
    BabyweekdetailPage,
    MomweekdetailPage
  ],

  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler},  
  AuthProvider,
  AngularFireAuth,
  ProfileProvider,
  DataProvider,
  ConnectivityServiceProvider,
  AdMobFree,
  Geolocation ]
})
export class AppModule {}
