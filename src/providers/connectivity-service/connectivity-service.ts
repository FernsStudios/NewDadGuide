import { Injectable } from '@angular/core';
import { Network } from 'ionic-native';
import { Platform } from 'ionic-angular';
declare var type;
@Injectable()
export class ConnectivityServiceProvider {
  onDevice: boolean;
  constructor(public platform: Platform){
    this.onDevice = this.platform.is('cordova'); //checks to see if an android device is online
  }
 
  isOnline(): boolean {
    if(this.onDevice && Network.type){
      return Network.type !== type.NONE;
    } else {
      return navigator.onLine; 
    }
  } //userstate for user that IS online
 
  isOffline(): boolean {
    if(this.onDevice && Network.type){
      return Network.type === type.NONE;
    } else {
      return !navigator.onLine;   
    }
  }//userstate for user that IS NOT online
  
}