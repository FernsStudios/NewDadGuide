﻿import { Component } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';
import { AddtodoPage } from '../addtodo/addtodo';
import { TodosDetailPage } from '../todos-detail/todos-detail';
import { DataProvider } from '../../providers/data/data';
import { ProfileProvider } from '../../providers/profile';

/**
 This currently only holds our to do list, which will be provided based on the TRIMESTER the user is in
 */
@Component({
  selector: 'page-todos',
  templateUrl: 'todos.html',
})

export class TodosPage {
//init variables
title;
secondtitle;
thirdtitle;
description;
seconddescription;
thirddescription;
userstate;
//init arrays 
public items = [];
public seconditems = [];
public thirditems = [];

    constructor(public navCtrl: NavController, public modalCtrl: ModalController, public dataService: DataProvider, public profileProvider: ProfileProvider, public navParams: NavParams) 
    {
        this.userstate = this.navParams.get('userstate');
        this.dataService.getData().then((todos) => {
            if (todos) {
                this.items = JSON.parse(todos);
                 
            }

        });
    }//end of constructor

    ionViewDidEnter() {
    

    if(this.userstate=='First Trimester')
    {
          this.items = [
                    {"title": "Research birthing methods", "description":""},
                     {"title": "Find a Doctor", "description":""},
                      {"title": "House hunting", "description":""},
                       {"title": "Choose a hospital", "description":""},
                        {"title": "Start saving!", "description":""},
                         {"title": "Find a midwife(if you want one)", "description":""},
                          {"title": "Birth announcements!", "description":""},
                           {"title": "Buy a baby name book", "description":""},
                ];
    }
    else if(this.userstate=='Second Trimester')
    {
          this.items = [
                    { "title": "Get your Vaccines!", "description": "Before your baby comes, its important for you and mom to have all your shots!" },
                    { "title": "Begin The Nursery!", "description": "With mom busy cooking a baby, its time for you to start cooking up nursery plans. " },
                    { "title": "Pick a Name for your baby!", "description": "Be careful when picking the name for your baby, and try not to listen to too much advice!" },
                   {"title": "Pick a car seat!", "description":""},
                    {"title": "Tell your work!", "description":""},
                     {"title": "Begin the baby-proofing!", "description":""},
                      {"title": "Pick out some diapers!", "description":""},
                ];
    }
    else if(this.userstate=='Third Trimester')
    {
          this.items = [
                    {"title": "Finish the nursery!", "description":""},
                     {"title": "Did you pick that name? Tell us!", "description":""},
                      {"title": "Post birth care(for boys)", "description":"circumcision?"},
                       {"title": "Hospital bags", "description":""},
                        {"title": "Know the route to the hospital", "description":""},
                         {"title": "Install the car seat", "description":""},
                          {"title": "Ready for breastfeeding?", "description":""},
                           {"title": "Read our newborn checklist", "description":""},
                   
                ];
    }
    }//end of if/else if block and Ionviewdidenter
    
    addItem()
    {

        let addModal = this.modalCtrl.create(AddtodoPage);

        addModal.onDidDismiss((item) => {

            if (item) {
                this.saveItem(item);
            }

        });

        addModal.present();

    }

    saveItem(item)
    {
        this.items.push(item);
        this.dataService.save(this.items);
    }

    viewItem(item)
    {
        this.navCtrl.push(TodosDetailPage, {
            item: item
        });
    }
}