﻿import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
/**
 * this pulls the full text string for the top dad update card OR the mom card and then pushes it to this page, allowing the user to read it easily
 */
@Component({
  selector: 'page-week-detail',
  templateUrl: 'week-detail.html',
})
export class WeekDetailPage {
userstate;
usertext;
    constructor(public navParams: NavParams) {}
    
    ionViewDidLoad() 
    {
        this.userstate = this.navParams.get('userstate');
        this.usertext = this.navParams.get('usertext');
    }
}
    



