﻿import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController,  AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth-provider';
import { EmailValidator } from '../../validators/email';
import { Signup } from '../signup/signup';
import { Login} from '../login/login';
/**
 * this uses a basic firebase function to send a auto generated email to the user if they need to reset their password, then they can click the link and be taken to a hosted page and change their password
 */
@Component({
  selector: 'page-resetpassword',
  templateUrl: 'resetpassword.html',
})
export class Resetpassword {
    public resetPasswordForm: FormGroup;
    constructor(public navCtrl: NavController, public navParams: NavParams, public authData: AuthProvider,
    public formBuilder: FormBuilder, public alertCtrl: AlertController, public loadingCtrl: LoadingController) 
    {
        this.resetPasswordForm = formBuilder.group({
            email: ['', Validators.compose([Validators.required,
            EmailValidator.isValid])],
        });
    }//end of constructor
    gotoLogin()
    {
        this.navCtrl.push(Login);
    }//pushes to login page
    goToSignup()
    {
        this.navCtrl.push(Signup);
    } //pushes to ResetPassword page

    resetPassword()
    {
        if (!this.resetPasswordForm.valid) {
            console.log(this.resetPasswordForm.value);
        } else {
            this.authData.resetPassword(this.resetPasswordForm.value.email)
                .then((user) => {
                    let alert = this.alertCtrl.create({
                        message: "We just sent you a reset link to your email",
                        buttons: [
                            {
                                text: "Ok",
                                role: 'cancel',
                                handler: () => { this.navCtrl.pop(); }
                            }
                        ]
                    });
                    alert.present();

                }, (error) => {
                    var errorMessage: string = error.message;
                    let errorAlert = this.alertCtrl.create({
                        message: errorMessage,
                        buttons: [{ text: "Ok", role: 'cancel' }]
                    });
                    errorAlert.present();
                });
        }
    }
  }




