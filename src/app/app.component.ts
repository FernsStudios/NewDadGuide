﻿import { Component, NgZone } from '@angular/core';
import { Platform } from 'ionic-angular';
import { HomePage } from '../pages/home/home';
import firebase from 'firebase';
import { IntroPage } from '../pages/intro/intro';
@Component({
  templateUrl: 'app.html'
})

export class MyApp {
    rootPage;
    zone: NgZone;
    constructor(platform: Platform) {
    this.zone = new NgZone({});
    firebase.initializeApp(
    {
         apiKey: "AIzaSyDGihL3oLfrYxJJ5Jw6UecweH5s_mLydek",
        authDomain: "the-new-dad-guide.firebaseapp.com",
        databaseURL: "https://the-new-dad-guide.firebaseio.com",
        projectId: "the-new-dad-guide",
        storageBucket: "the-new-dad-guide.appspot.com",
        messagingSenderId: "321744267966"
    }); //Set Firebase Variables
    //If they have a firebase account registered with us, it will set root page to HomePage. Otherwise, it sets them back to our Intro page. 
    //This triggers on entry, and on any state change within the Auth User or Firebase Db
      const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
          this.zone.run(() => {
              if (!user) {
                  this.rootPage = IntroPage;
                  unsubscribe();
              } else {
                  this.rootPage = HomePage;
                  unsubscribe();
              }
          });
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
     
});
}
}
