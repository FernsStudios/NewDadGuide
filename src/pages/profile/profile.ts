﻿import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ProfileProvider } from '../../providers/profile';
import { AuthProvider } from '../../providers/auth-provider';
import { Login } from '../login/login';
import { HomePage } from '../home/home';
/**
 * pulls the profile info from their node in our firebase database and then populates it 
 * it then allows them to change this info at will, although some will need them to re log
 */
@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
})
export class Profile {
    public userProfile: any;
    public birthDate: string;
    dueDate: any;
    databaseDate: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    public profileProvider: ProfileProvider, public authProvider: AuthProvider) {}

    ionViewDidEnter()
    {
        this.profileProvider.getUserProfile().then(profileSnap => {
            this.userProfile = profileSnap;
            this.birthDate = this.userProfile.birthDate;
            this.databaseDate = new Date(this.userProfile.dueDate).toDateString();
        });
    }

    goToHome()
    {
        this.navCtrl.push(HomePage)
    }

    logOut()
    {
        this.authProvider.logoutUser().then(() => {
        this.navCtrl.setRoot(Login);
        });
    }

    updateName()
    {
        let alert = this.alertCtrl.create({
            message: "Your first name & last name",
            inputs: [
                {
                    name: 'firstName',
                    placeholder: 'Your first name',
                    value: this.userProfile.firstName
                },
                {
                    name: 'lastName',
                    placeholder: 'Your last name',
                    value: this.userProfile.lastName
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                },
                {
                    text: 'Save',
                    handler: data => {
                        this.profileProvider.updateName(data.firstName, data.lastName);
                    }
                }
            ]
        });
        alert.present();
    }

    updatemomName() 
    {
        let alert = this.alertCtrl.create({
            message: "Your first name & last name",
            inputs: [
                {
                    name: 'momfirst',
                    placeholder: 'Your Mom-To-Bes name!',
                    value: this.userProfile.momfirst
                },
               
            ],
            buttons: [
                {
                    text: 'Cancel',
                },
                {
                    text: 'Save',
                    handler: data => {
                        this.profileProvider.updateMomName(data.momfirst);
                    }
                }
            ]
        });
        alert.present();
    }

    updateDOB(birthDate) 
    {
        this.profileProvider.updateDOB(birthDate);
    }

    updateDueDate(dueDate) 
    {
        this.dueDate = new Date(dueDate);
        this.databaseDate = this.dueDate.getTime();
        this.profileProvider.updateDueDate(this.databaseDate);
        this.authProvider.logoutUser().then(() => {
            this.navCtrl.setRoot(Login);
        });
    }
    updateEmail() 
    {
        let alert = this.alertCtrl.create({
            inputs: [
                {
                    name: 'newEmail',
                    placeholder: 'Your new email',
                },
                {
                    name: 'password',
                    placeholder: 'Your password',
                    type: 'password'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                },
                {
                    text: 'Save',
                    handler: data => {
                        this.profileProvider.updateEmail(data.newEmail, data.password);
                    }
                }
            ]
        });
        alert.present();
    }

    updatePassword() 
    {
        let alert = this.alertCtrl.create({
            inputs: [
                {
                    name: 'newPassword',
                    placeholder: 'Your new password',
                    type: 'password'
                },
                {
                    name: 'oldPassword',
                    placeholder: 'Your old password',
                    type: 'password'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                },
                {
                    text: 'Save',
                    handler: data => {
                        this.profileProvider.updatePassword(data.newPassword, data.oldPassword);
                    }
                }
            ]
        });
        alert.present();
    }
    
}