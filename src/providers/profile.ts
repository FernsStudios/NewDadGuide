﻿import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import firebase from 'firebase';
/*
  Profile page, calls the users info from the firebase database Userid node, and their auto email/pass used when they signed up
!!!!!!!!!Right now, they have to input this the first time in profile, then it will autopopulate when they visit it again
*/
@Injectable()

export class ProfileProvider {

  constructor() {}

  getUserProfile(): Promise<any>
  {
      return new Promise((resolve, reject) =>
      {
          firebase.database().ref('/userProfile')
              .child(firebase.auth().currentUser.uid)
              .on('value', data => {
                  resolve(data.val());
              });
      });
  }// end of getUserProfile

  updateName(firstName: string, lastName: string): firebase.Promise<any>
  {
      return firebase.database().ref('/userProfile')
          .child(firebase.auth().currentUser.uid).update({
              firstName: firstName,
              lastName: lastName,
          });
  }//end of updateName

  updateMomName(momfirst: string): firebase.Promise<any>
  {
      return firebase.database().ref('/userProfile')
          .child(firebase.auth().currentUser.uid).update({
              momfirst: momfirst,
             
          });
  }//end of updateMomName
  
  updateGender(gender: string): firebase.Promise<any> {
      return firebase.database().ref('/userProfile')
          .child(firebase.auth().currentUser.uid).update({
              usergender: gender,
          });
  }//end of updateGender

  updateDOB(birthDate: string): firebase.Promise<any>
  {
      return firebase.database().ref('/userProfile')
          .child(firebase.auth().currentUser.uid).update({
              birthDate: birthDate,
          });
  }//end of updateDOB

  updateDueDate(dueDate: string): firebase.Promise<any> {
      return firebase.database().ref('/userProfile')
          .child(firebase.auth().currentUser.uid).update({
              dueDate: dueDate,
          });
  }//end of updateDOB


  updateEmail(newEmail: string, password: string): firebase.Promise<any> {
      const credential = firebase.auth.EmailAuthProvider
          .credential(firebase.auth().currentUser.email, password);

      return firebase.auth().currentUser.reauthenticate(credential)
          .then(user => {
              firebase.auth().currentUser.updateEmail(newEmail).then(user => {
                  firebase.database().ref('/userProfile')
                      .child(firebase.auth().currentUser.uid).update({ email: newEmail });
              });
          });
  }//end of Update email, !!!!!this will change it with the authentication and at their node in the database under /userProfile


  updatePassword(newPass: string, oldPassword: string): firebase.Promise<any> {
      const credential = firebase.auth.EmailAuthProvider
          .credential(firebase.auth().currentUser.email, oldPassword);

      return firebase.auth().currentUser.reauthenticate(credential)
          .then(user => {
              firebase.auth().currentUser.updatePassword(newPass).then(user => {
                  console.log("Password Changed");
              }, error => {
                  console.log(error);
              });
          });
  }//end of updatePass

}//end of Profile
