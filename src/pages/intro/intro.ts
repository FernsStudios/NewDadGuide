﻿import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Signup } from '../signup/signup';
import { Login } from '../login/login';
/**
 * Intro slides on first open of app, after user has created a log in they will not see this page and will go straight to home page
 */
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  gotoLogin()
  {
      this.navCtrl.push(Login);
  }//pushes to login page
  goToSignUp()
  {
 this.navCtrl.push(Signup); 
  }//pushes to signup

  createAccount()
  {
    this.navCtrl.push(Signup);
  }

}
